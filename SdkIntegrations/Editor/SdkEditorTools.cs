﻿using System.Linq;
using UnityEditor;
using UnityEditor.Build;

namespace DefaultNamespace
{
    #if UNITY_EDITOR
    public static class SdkEditorTools
    {
        private const string YandexSdkName = "USE_YANDEX_GAMES_SDK";
        private const string YandexTemplateName = "PROJECT:PluginYG";
        private const string CrazySdkName = "USE_CRAZY_GAMES_SDK";
        private const string CrazyTemplateName = "PROJECT:Crazy_2020";
        
        [MenuItem("Tools/" + nameof(SwitchToYandex))]
        public static void SwitchToYandex()
        {
            SwitchToSdk(YandexSdkName, YandexTemplateName);
            PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Disabled;
        }
        
        [MenuItem("Tools/" + nameof(SwitchToCrazy))]
        public static void SwitchToCrazy()
        {
            SwitchToSdk(CrazySdkName, CrazyTemplateName);
            PlayerSettings.WebGL.compressionFormat = WebGLCompressionFormat.Brotli;
        }

        private static void SwitchToSdk(string sdk, string template)
        {
            PlayerSettings.GetScriptingDefineSymbols(NamedBuildTarget.WebGL, out var defines);
            var definesWithoutSdk = defines.Where(x => x != YandexSdkName && x != CrazySdkName).ToList();
            definesWithoutSdk.Add(sdk);
            PlayerSettings.SetScriptingDefineSymbols(NamedBuildTarget.WebGL, definesWithoutSdk.ToArray());
            PlayerSettings.SetPropertyString("template", template, BuildTargetGroup.WebGL);
        }
    }
    #endif
}