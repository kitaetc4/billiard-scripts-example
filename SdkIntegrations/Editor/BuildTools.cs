﻿using System.IO;
using DefaultNamespace;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace SdkIntegrations.Editor
{
    public static class BuildTools
    {
        [MenuItem("Tools/Build for YG and CG")]
        public static void BuildAll()
        {
            SdkEditorTools.SwitchToYandex();
            Build("_YandexGames");
            SdkEditorTools.SwitchToCrazy();
            Build("_CrazyGames");
        }

        private static void Build(string nameAdd)
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            BuildPlayerWindow.DefaultBuildMethods.GetBuildPlayerOptions(buildPlayerOptions);

            // Get your build version here
            string version = "V1.0.0";
            string path = Path.Join(buildPlayerOptions.locationPathName, version + nameAdd);

            Directory.CreateDirectory(path);
            buildPlayerOptions.locationPathName = path;

            BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
            BuildSummary summary = report.summary;

            if (summary.result == BuildResult.Succeeded)
            {
                Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
            }

            if (summary.result == BuildResult.Failed)
            {
                Debug.Log("Build failed");
            }
        }
    }
}