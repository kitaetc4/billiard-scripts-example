﻿using System;
using CrazyGames;
using Cysharp.Threading.Tasks;
using Scripts.Services;
using VContainer.Unity;

namespace SdkIntegrations.Crazy
{
    public class AdsPlacementOnGameplayLoaded : IStartable
    {
        public AdsPlacementOnGameplayLoaded(ISceneLoaderService sceneLoader)
        {
            sceneLoader.OnGameplayLoaded += OnSceneChange;
        }

        private void OnSceneChange()
        {
            CrazyAds.Instance.beginAdBreak();
        }

        public void Start() // без этого не пашет, не трогать
        {
            UniTask.Delay(TimeSpan.FromMilliseconds(10));
        }
    }
}