﻿using System;

namespace SdkIntegrations
{
    public interface ISdk
    {
        public event Action GetDataEvent;
        public event Action<string> SwitchLangEvent;
        event Action<int> RewardVideoEvent;
        void RewardedShow(int i);
        public bool SDKEnabled { get; }
        public bool AuthCompleted { get; }
        public string PlayerPhotoUrl { get; }
        public bool IsUnauthorized { get; }
        public bool IsAnonymous { get; }
        string PlayerName { get; }
        void ResetSaveProgress();
        void NewLeaderboardScores(string leaderboard, int wins);
        void GameplayStart();
        void GameplayStop();
    }
}