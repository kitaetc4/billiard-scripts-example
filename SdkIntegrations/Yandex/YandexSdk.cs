﻿using System;
using UnityEngine;
using YG;

namespace SdkIntegrations.Yandex
{
    public class YandexSdk : ISdk
    {
        public YandexSdk()
        {
            Debug.Log("YandexSdk()");
            YandexGame.GetDataEvent += () => GetDataEvent?.Invoke();
            YandexGame.SwitchLangEvent += lang => SwitchLangEvent?.Invoke(lang);            
            YandexGame.RewardVideoEvent += id => RewardVideoEvent?.Invoke(id);
        }
        
        public event Action GetDataEvent;
        public event Action<string> SwitchLangEvent;
        public event Action<int> RewardVideoEvent;
        public void RewardedShow(int i)
        {
            YandexGame.Instance._RewardedShow(i);
        }

        public bool SDKEnabled => YandexGame.SDKEnabled;
        public bool AuthCompleted => YandexGame.auth;
        public string PlayerPhotoUrl => YandexGame.playerPhoto;
        public bool IsUnauthorized => YandexGame.playerName == "unauthorized";
        public bool IsAnonymous => YandexGame.playerName == "anonymous";
        public string PlayerName => YandexGame.playerName;
        public void ResetSaveProgress()
        {
            YandexGame.ResetSaveProgress();
        }

        public void NewLeaderboardScores(string leaderboard, int wins)
        {
            YandexGame.NewLeaderboardScores("LeaderBoard", wins);
        }

        public void GameplayStart()
        {
            Debug.Log(nameof(GameplayStart));
        }

        public void GameplayStop()
        {
            Debug.Log(nameof(GameplayStop));
        }
    }
}