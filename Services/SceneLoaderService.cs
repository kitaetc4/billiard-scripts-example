using System;
using Cysharp.Threading.Tasks;
using SdkIntegrations;
using UnityEngine;
using UnityEngine.SceneManagement;
using VContainer;
#if USE_CRAZY_GAMES_SDK
using CrazyGames;
#endif

namespace Scripts.Services
{
    public class SceneLoaderService : ISceneLoaderService
    {
        [Inject] private readonly ISdk _sdk;
        private readonly string _gameplaySceneName = "GameScene";
        private readonly string _menuSceneName = "MainMenu";
        public event Action OnGameplayLoaded;
        
        public UniTask LoadGameplayScene()
        {
            var op = SceneManager.LoadSceneAsync(_gameplaySceneName);
            op.completed += _ => _sdk.GameplayStart();
            op.completed += _ => OnGameplayLoaded?.Invoke();
            return op.ToUniTask();
        }

        public UniTask LoadMenuScene()
        {
            var op = SceneManager.LoadSceneAsync(_menuSceneName);
            op.completed += _ => _sdk.GameplayStop();
            return op.ToUniTask();
        }

    }

    public interface ISceneLoaderService
    {
        public UniTask LoadGameplayScene();
        public UniTask LoadMenuScene();
        public event Action OnGameplayLoaded;
    }
}