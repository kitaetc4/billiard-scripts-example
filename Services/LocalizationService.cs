﻿using System;
using TMPro;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

namespace Scripts.Services
{
    public static class LocalizationService
    {
        private const string LocalizationTableName = "TextTableLocalization";

        public static void GetTranslatedStringAsync(string key, Action<string> result)
        {
            LocalizationSettings.StringDatabase
                    .GetTableEntryAsync(LocalizationTableName, key).Completed
                += x =>
                {
                    if (x.Result.Entry != null)
                    {
                        result((x.Result.Entry.GetLocalizedString()));
                        return;
                    }
                    result(key);
                };
        }

        public static void GetTranslatedStringAsync(LocalizedString localizedString, TextMeshProUGUI target)
        {
            localizedString.GetLocalizedStringAsync().Completed += t => target.text = t.Result;
            localizedString.RefreshString();
        }
    }
}