using Core;
using Core.Configs;
using Scripts.Ui;
using UnityEngine;
using VContainer;

namespace Scripts.Services
{
    public interface IMatchLoaderService
    {
        public void LoadMatchPvp(bool aim);
        public void LoadMatchPveVs(ParticipantDescription enemy);
        public void LoadMatch(MissionDescription missionDescription);
        public void RestartCurrentMatch();
    }
    
    public class MatchLoaderService : IMatchLoaderService
    {
        private readonly string _pvpPlayer2 = "PvpPlayer2";
        
        [Inject] private readonly IConfig<string, ParticipantDescription> _participantsConfig;
        [Inject] private readonly MatchModel _matchModel;
        [Inject] private readonly ISceneLoaderService _sceneLoaderService;
        [Inject] private readonly IWindowManager _windowManager;
        [Inject] private readonly ParticipantDescriptionSdkPlayerData _sdkPlayerDesc;


        public void LoadMatchPvp(bool aimLines)
        {
            var player2 = _participantsConfig[_pvpPlayer2];
            LoadMatch(new MatchConfig(_sdkPlayerDesc, player2, aimLines));
        }

        public void LoadMatchPveVs(ParticipantDescription enemy)
        {
            LoadMatch(new MatchConfig(_sdkPlayerDesc, enemy, true)); ;
        }

        public void LoadMatch(MissionDescription missionDescription)
        {
            LoadMatch(new MatchConfig(_sdkPlayerDesc, missionDescription.Enemy, missionDescription.AimLines));
            _matchModel.Mission = missionDescription;
        }

        public void RestartCurrentMatch()
        {
            if (_matchModel == null) return;
            _windowManager.CloseAllWindows();
            LoadMatch(_matchModel.Match);
        }

        private void LoadMatch(MatchConfig matchConfig)
        {
            _matchModel.Match = matchConfig;
            _matchModel.Mission = ScriptableObject.CreateInstance<MissionDescription>();
            _sceneLoaderService.LoadGameplayScene();
        }
    }
}