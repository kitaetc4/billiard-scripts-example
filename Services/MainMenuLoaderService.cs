using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Scripts.Ui;
using Scripts.Ui.Windows;
using VContainer;

namespace Scripts.Services
{
    public class MainMenuLoaderService : IMainMenuLoaderService
    {
        [Inject] private readonly ISceneLoaderService _sceneLoaderService;
        [Inject] private readonly IWindowManager _windowManager;
    
        public async Task LoadMainMenu()
        {
            _windowManager.CloseAllWindows();
            await _sceneLoaderService.LoadMenuScene();
            await UniTask.Yield(PlayerLoopTiming.PostLateUpdate);
            _windowManager.OpenWindow<MainMenuWindow>();
        }
    }

    public interface IMainMenuLoaderService
    {
        Task LoadMainMenu();
    }
}