using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(FloatList))]
    public class FloatList : ScriptableObjectList<Sprite>
    {
    }
}