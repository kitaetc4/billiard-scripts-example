using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(SpriteList))]
    public class StringList : ScriptableObjectList<Sprite>
    {
    }
}
