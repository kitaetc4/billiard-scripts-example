using System.Linq;
using UnityEngine;

namespace Core.Configs
{
    public class LookupTableManager
    {
        public readonly InterpolatedTable CollisionEnergy;
        public readonly InterpolatedTable StrikePower;
        
        public readonly CollisionEnergyLookupTable CollisionEnergyConfig;
        public readonly StrikePowerLookupTable StrikePowerConfig; 
        
        public LookupTableManager()
        {
            CollisionEnergyConfig = Resources.Load<CollisionEnergyLookupTable>("Configs/CollisionEnergyLookupTable");
            StrikePowerConfig = Resources.Load<StrikePowerLookupTable>("Configs/StrikePowerLookupTable");

            CollisionEnergy = new InterpolatedTable(
                CollisionEnergyConfig.Table.Select(e => e.CollisionAngle).ToArray(),
                CollisionEnergyConfig.Table.Select(e => e.VelocityTransfered).ToArray()
            );
            StrikePower = new InterpolatedTable(
                StrikePowerConfig.Table.Select(e => e.StrikePower).ToArray(),
                StrikePowerConfig.Table.Select(e => e.TotalDistance).ToArray()
            );
        }
    }
}