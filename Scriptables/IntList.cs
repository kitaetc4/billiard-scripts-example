using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(IntList))]
    public class IntList : ScriptableObjectList<Sprite>
    {
    }
}