using System;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

namespace Core.Configs
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(MissionDescription))]
    public class MissionDescription : ScriptableObject
    {
        public IParticipantDescription Enemy => _enemy;
        public Currency WinReward => _winReward;
        public Currency EnterPrice => _enterPrice;
        public Currency WinsRequiredToEnter => _winsRequiredToEnter;
        public AiSettings Difficulty => _difficulty;
        public bool AimLines => _aimLines;
        public bool PlayerStrikesFirst => _playerStrikesFirst;

        [SerializeField, Expandable] private ParticipantDescription _enemy;
        [SerializeField] private Currency _winReward = new();
        [SerializeField] private Currency _enterPrice = new();
        [FormerlySerializedAs("_enterWin")] [SerializeField] private Currency _winsRequiredToEnter = new();
        [SerializeField] private AiSettings _difficulty = new();
        [SerializeField] private bool _aimLines = true;
        [SerializeField] private bool _playerStrikesFirst = true;

    }

    [Serializable]
    public class AiSettings
    {
        [field: SerializeField] public float BotAddRandomStrikeAngle;
        [field: SerializeField] public float InaccuracyMultiplierForEveryConsecutiveGoal { get; private set; }
        [field: SerializeField] public float InaccuracyMultiplierOnLastGoal { get; private set; }
        [field: SerializeField] public float InaccuracyMultiplierWhenPlayerIsOnLastBall { get; private set; }
    }
}