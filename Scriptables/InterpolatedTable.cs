using Core.Utils;

namespace Core.Configs
{
    public class InterpolatedTable
    {
        private readonly float[] _xValues;
        private readonly float[] _yValues;

        public InterpolatedTable(float[] xValues, float[] yValues)
        {
            _xValues = xValues;
            _yValues = yValues;
        }
        
        public float CalculateY(float xIn)
        {
            var range = FindRangeForX(xIn);
            return MathUtils.Remap(xIn, range.x1, range.x2, range.y1, range.y2);
        }
        public float CalculateX(float yIn)
        {
            var range = FindRangeForY(yIn);
            return MathUtils.Remap(yIn, range.y1, range.y2, range.x1, range.x2);
        }
        
        private Pair FindRangeForX(float targetX)
        {
            for (int i = 1; i < _xValues.Length; i++)
            {
                var x = _xValues[i];
                if (x > targetX)
                {
                    return new Pair
                    {
                        x1 = _xValues[i-1],
                        y1 = _yValues[i-1],
                        x2 = _xValues[i],
                        y2 = _yValues[i],
                    };
                }
            }

            var lastI = _xValues.Length - 1;
            return new Pair
            {
                x1 = _xValues[lastI-1],
                y1 = _yValues[lastI-1],
                x2 = _xValues[lastI],
                y2 = _yValues[lastI],
            };
        }
        private Pair FindRangeForY(float targetY)
        {
            for (int i = 1; i < _yValues.Length; i++)
            {
                var y = _yValues[i];
                if (y > targetY)
                {
                    return new Pair
                    {
                        x1 = _xValues[i-1],
                        y1 = _yValues[i-1],
                        x2 = _xValues[i],
                        y2 = _yValues[i],
                    };
                }
            }

            var lastI = _xValues.Length - 1;
            return new Pair
            {
                x1 = _xValues[lastI-1],
                y1 = _yValues[lastI-1],
                x2 = _xValues[lastI],
                y2 = _yValues[lastI],
            };
        }

        private class Pair
        {
            public float x1;
            public float y1;
            public float x2;
            public float y2;
        }
    }
}