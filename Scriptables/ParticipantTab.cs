﻿namespace Core.Configs
{
    public enum ParticipantTab
    {
        Beginner,
        Advanced,
        Expert
    }
}