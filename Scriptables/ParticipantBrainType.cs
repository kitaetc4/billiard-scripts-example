﻿namespace Core.Configs
{
    public enum ParticipantBrainType
    {
        Bot,
        PlayerControlled,
    }
}