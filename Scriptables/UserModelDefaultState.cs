using Core.Configs;
using UnityEngine;

namespace Scripts.Model
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(UserModelDefaultState))]
    public class UserModelDefaultState : ScriptableObject
    {
        [field: SerializeField] public UserModel Model { get; private set; }
    }
}