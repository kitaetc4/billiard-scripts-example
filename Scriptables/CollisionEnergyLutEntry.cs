using System;
using UnityEngine;

namespace Core.Configs
{
    [Serializable]
    public class CollisionEnergyLutEntry
    {
        public float CollisionAngle => _collisionAngle;

        public float VelocityLost => _velocityLost;

        public float VelocityTransfered => _velocityTransfered;
        
        /// <summary>
        /// угол между траекторией движения шара и направлением, которое он задаст другому шару при столкновении
        /// </summary>
        [SerializeField] private float _collisionAngle;
        /// <summary>
        /// доля скорости, теряемая после столкновения
        /// </summary>
        [SerializeField] private float _velocityLost;

        /// <summary>
        /// доля скорости, отдаваемая другому шару после столкновения
        /// </summary>
        [SerializeField] private float _velocityTransfered; 
    }
}