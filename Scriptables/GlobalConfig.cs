﻿using Core;
using Core.Configs;
using Core.Controllers;
using UnityEngine;

namespace Scripts.Scriptables
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(GlobalConfig))]
    public class GlobalConfig : ScriptableObject
    {
        [field: SerializeField] public float BallYpositionToConsiderFallen { get; private set; } = -2f;
        
        [field: SerializeField] public ParticipantDescription DefaultPlayerDesc { get; private set; }
        [field: SerializeField] public DynamicPhysicsTimestepController.Settings PhysicsSettings { get; private set; }
        [field: SerializeField] public float BallRadius { get; private set; } = 0.033f;
    }
}