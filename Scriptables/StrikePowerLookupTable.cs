using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(StrikePowerLookupTable))]
    public class StrikePowerLookupTable : ScriptableObject
    {
        public List<StrikePowerLutEntry> Table => _table;
        [SerializeField] private List<StrikePowerLutEntry> _table;

        public void SetTable(List<StrikePowerLutEntry> table)
        {
            _table = table;
        }
    }

    [Serializable]
    public class StrikePowerLutEntry
    {
        public float StrikePower
        {
            get => _strikePower;
            set => _strikePower = value;
        }
        
        public float TotalDistance
        {
            get => _totalDistance;
            set => _totalDistance = value;
        }

        public List<DistanceByTime> DistanceByTime => _distanceByTime;

        [SerializeField] private float _strikePower;
        [SerializeField] private List<DistanceByTime> _distanceByTime = new();
        [SerializeField] private float _totalDistance;
    }

    [Serializable]
    public class DistanceByTime
    {
        public float TimePassed => _timePassed;
        public float Distance => _distance;
        
        [SerializeField] private float _timePassed;
        [SerializeField] private float _distance;

        public DistanceByTime(float timePassed, float totalDistance)
        {
            _timePassed = timePassed;
            _distance = totalDistance;
        }
    }
}