using System.Collections.Generic;
using UnityEngine;

namespace Core.Configs
{
    [CreateAssetMenu(menuName = GameName.Name + nameof(CollisionEnergyLookupTable))]
    public class CollisionEnergyLookupTable : ScriptableObject
    {
        public List<CollisionEnergyLutEntry> Table => _table;
        [SerializeField] private List<CollisionEnergyLutEntry> _table;
    }
}   