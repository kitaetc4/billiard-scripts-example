using System.Collections.Generic;
using UnityEngine;

namespace Core.Configs
{
    public abstract class ScriptableObjectList<T> : ScriptableObject
    {
        [SerializeField] private List<T> _items = new();
        public List<T> Items => _items;
    }
}