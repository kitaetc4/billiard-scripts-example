using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public static class LinqExtensions
    {
        /// <summary>
        /// Linq get random
        /// </summary>
        public static TSource GetRandomElement<TSource>(this IList<TSource> source)
        {
            var randIndex = UnityEngine.Random.Range(0, source.Count);
            return source[randIndex];
        }
        
        /// <summary>
        /// Linq ignore element
        /// </summary>
        public static IEnumerable<TSource> Without<TSource>(this IEnumerable<TSource> source, TSource elementToRemove)
        {
            return source.Except(new[] { elementToRemove });
        }
        
        /// <summary>
        /// https://codereview.stackexchange.com/questions/227211/linq-extension-methods-minby-and-maxby
        /// </summary>
        private static TSource ArgBy<TSource, TKey>(
            this IEnumerable<TSource> source, 
            Func<TSource, TKey> keySelector, 
            Func<(TKey Current, TKey Previous), bool> predicate)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));
            if (predicate == null) throw new ArgumentNullException(nameof(predicate));

            var value = default(TSource);
            var key = default(TKey);

            if (value == null)
            {
                foreach (var other in source)
                {
                    if (other == null) continue;
                    var otherKey = keySelector(other);
                    if (otherKey == null) continue;
                    if (value == null || predicate((otherKey, key)))
                    {
                        value = other;
                        key = otherKey;
                    }

                }
                return value;
            }
            else
            {
                bool hasValue = false;
                foreach (var other in source)
                {
                    var otherKey = keySelector(other);
                    if (otherKey == null) continue;

                    if (hasValue)
                    {
                        if (predicate((otherKey, key))) 
                        {
                            value = other;
                            key = otherKey;
                        }
                    }
                    else
                    {
                        value = other;
                        key = otherKey;
                        hasValue = true;
                    }
                }
                if (hasValue) return value;
                throw new InvalidOperationException("Sequence contains no elements");
            }
        }
        
        /// <summary>
        /// Наименьший элемент по критерию keySelector
        /// Не будем ждать, пока юнити поддержит новую версию языка, напишем это сами
        /// </summary>
        public static TSource MinBy<TSource, TKey>(
            this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector,
            IComparer<TKey> comparer = null)
        {
            if (comparer == null) comparer = Comparer<TKey>.Default;
            return source.ArgBy(keySelector, lag => comparer.Compare(lag.Current, lag.Previous) < 0);
        }

        /// <summary>
        /// Наибольший элемент по критерию keySelector
        /// Не будем ждать, пока юнити поддержит новую версию языка, напишем это сами
        /// </summary>
        public static TSource MaxBy<TSource, TKey>(
            this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector,
            IComparer<TKey> comparer = null)
        {
            if (comparer == null) comparer = Comparer<TKey>.Default;
            return source.ArgBy(keySelector, lag => comparer.Compare(lag.Current, lag.Previous) > 0);
        }

        /// <summary>
        /// Full cycle foreach
        /// </summary>
        public static IEnumerable<TSource> Foreach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
        {
            var list = source.ToList();
            foreach (var element in list)
            {
                action(element);
            }
            return list;
        }
        
        /// <summary>
        /// Shuffles the enumerable using the Fisher-Yates-Durstenfeld method.
        /// </summary>
        /// <returns>A new enumerable with all the elements in the original shuffled.</returns>
        public static IEnumerable<T> GetShuffled<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.GetShuffled(new System.Random());
        }

        /// <summary>
        /// Shuffles the enumerable using the Fisher-Yates-Durstenfeld method.
        /// </summary>
        /// <param name="rnd">Pseudo random number generator to be used.</param>
        /// <returns>A new enumerable with all the elements in the original shuffled.</returns>
        private static IEnumerable<T> GetShuffled<T>(this IEnumerable<T> source, System.Random rnd)
        {
            List<T> buffer = source.ToList();
            for (int i = 0; i < buffer.Count; i++)
            {
                int j = rnd.Next(i, buffer.Count);
                yield return buffer[j];

                buffer[j] = buffer[i];
            }
        }
        
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !source.Any();
        }


    }
}