using UnityEngine;

namespace Core.Utils
{
    public static class Vector3Utils
    {
        public static Vector2 ToXz(this Vector3 v3)
        {
            return new Vector2(v3.x, v3.z);
        }
    }
}