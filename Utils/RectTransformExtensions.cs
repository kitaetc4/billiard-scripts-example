using UnityEngine;

namespace Scripts.Utils
{
    public static class RectTransformExtensions
    {
        public static void DestroyChildren(this RectTransform rectTransform)
        {
            if (rectTransform == null) return;
            for (int i = rectTransform.childCount - 1; i >= 0; i--)
            {
                Object.Destroy(rectTransform.GetChild(i).gameObject);
            }
        }
    }
}