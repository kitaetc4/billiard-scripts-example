using UnityEngine;
using VContainer;

namespace Core.Utils
{
    public static class ContainerBuilderExtensions
    {
        public static RegistrationBuilder AsSelfAndInterfaces(this RegistrationBuilder rb)
        {
            return rb.AsSelf().AsImplementedInterfaces();
        }
        
        public static RegistrationBuilder RegisterInHierarchyOrCreateFromPrefab<T>(this IContainerBuilder builder,
            T prefab)
            where T : Object
        {
            var instance = Object.FindObjectOfType<T>();
            if (instance == null)
            {
                instance = Object.Instantiate(prefab);
            }
            return builder.RegisterInstance(instance);
        }
        
        public static RegistrationBuilder RegisterInHierarchyOrCreateFromResources<T>(this IContainerBuilder builder,
            string prefabPath)
            where T : Object
        {
            var prefab = Resources.Load<T>(prefabPath);
            return builder.RegisterInHierarchyOrCreateFromPrefab(prefab);
        }
    }
}