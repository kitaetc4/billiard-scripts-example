using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.Utils
{
    public static class UiUtils
    {
        private static LayerMask UILayer = LayerMask.NameToLayer("UI");
     
        
        /// <summary>
        /// Returns 'true' if we touched or hovering on Unity UI element.
        /// </summary>
        public static bool IsPointerOverUIElement(int layer = 0)
        {
            if (layer == 0) layer = UILayer;
            return IsPointerOverUIElement(GetEventSystemRaycastResults(), layer);
        }
        
        /// <summary>
        /// Returns 'true' if we touched or hovering on Unity UI element.
        /// </summary>
        private static bool IsPointerOverUIElement(List<RaycastResult> eventSystemRaysastResults, int layer)
        {
            for (int index = 0; index < eventSystemRaysastResults.Count; index++)
            {
                RaycastResult curRaycastResult = eventSystemRaysastResults[index];
                if (curRaycastResult.gameObject.layer == layer)
                    return true;
            }
            return false;
        }
 
 
        /// <summary>
        /// Gets all event system raycast results of current mouse or touch position.
        /// </summary>
        private static List<RaycastResult> GetEventSystemRaycastResults()
        {
            PointerEventData eventData = new PointerEventData(EventSystem.current);
            eventData.position = Input.mousePosition;
            List<RaycastResult> raysastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, raysastResults);
            return raysastResults;
        }
    }
}