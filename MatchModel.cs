using Core;
using Core.Configs;

namespace Scripts
{
    public class MatchModel : IMatchModel
    {
        public Currency WinReward => Mission != null ? Mission.WinReward : new Currency();
        
        public IParticipantDescription Player => Match.Player1.Brain == ParticipantBrainType.PlayerControlled ? Match.Player1 : Match.Player2;
        public IParticipantDescription Bot => Match.Player1.Brain == ParticipantBrainType.Bot ? Match.Player1 : Match.Player2;
        
        public MatchConfig Match { get; set; }
        public MissionDescription Mission { get; set; }
    }

    public interface IMatchModel
    {
        public IParticipantDescription Player { get; }
        public IParticipantDescription Bot { get; }
        public MatchConfig Match { get; }
        public Currency WinReward { get; }
        public MissionDescription Mission { get; }
    }
}