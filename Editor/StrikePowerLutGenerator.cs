using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Configs;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEditor.Build.Content;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts.Editor
{
    public static class StrikePowerLutGenerator
    {
        [MenuItem(GameName.Name + nameof(GenerateStrikePowerLut))]
        public static async UniTaskVoid GenerateStrikePowerLut()
        {
            
            var table = Resources.Load<StrikePowerLookupTable>("Configs/StrikePowerLookupTable");
            if (table == null)
            {
                Debug.LogError("cant find Configs/StrikePowerLookupTable");
                return;
            }            

            if (!Application.isPlaying)
                EditorApplication.ExecuteMenuItem("Edit/Play");

            var activeScene = EditorSceneManager.GetActiveScene();
            if (activeScene.name != "StrikePowerLutGeneratorScene")
            {
                Debug.LogError("need to open scene Assets/Scenes/StrikePowerLutGeneratorScene.unity");
            }

            var balls = GameObject.FindObjectsOfType<Ball>();

            var maxPower = 100;
            var minPower = 10;
            var powerStep = 10;

            var strikePowers = new Dictionary<Ball, int>();
            var lut = new Dictionary<int, StrikePowerLutEntry>();

            var ballsProvider = new BallsProvider();
            ballsProvider.Initialize();
            var settleController = new BallSettleController(ballsProvider);
            settleController.Unsettle();

            for (int i = 0; i < 10; i++)
            {
                var power = powerStep * (i + 1);
                var ball = balls[i];
                strikePowers[ball] = power;
                lut[power] = new StrikePowerLutEntry();
                lut[power].StrikePower = power;
                BallStrikeUtils.StrikeBall(ball, Vector3.forward, power);
                var t = UniTask.Create(() => WriteBallDistance(ball, lut[power], settleController));
            }

            await UniTask.WaitForEndOfFrame();
            var delay = 0.5f;
            var timePassed = 0f;
            while (!AllSettled(balls, settleController))
            {
                await UniTask.Delay(TimeSpan.FromSeconds(delay));
                timePassed += delay;
                foreach (var b in balls)
                {
                    if (settleController.BallIsSettled(b)) continue;
                    var power = strikePowers[b];
                    lut[power].DistanceByTime.Add(new DistanceByTime(timePassed, lut[power].TotalDistance));
                }
            }

            table.SetTable(lut.Values.ToList());
            Debug.Log("Lut generated!");
        }

        private static bool AllSettled(IEnumerable<Ball> balls, BallSettleController settleController)
        {
            foreach (var b in balls)
            {
                if (!settleController.BallIsSettled(b)) return false;
            }

            return true;
        }

        private static async UniTask WriteBallDistance(Ball ball, 
            StrikePowerLutEntry strikePowerLutEntry, 
            BallSettleController settleController)
        {
            await UniTask.WaitForEndOfFrame();

            while (!settleController.BallIsSettled(ball))
            {
                var pos = ball.transform.position;
                await UniTask.WaitForEndOfFrame();
                var newPos = ball.transform.position;
                strikePowerLutEntry.TotalDistance += Vector3.Distance(pos, newPos);
            }
        }
    }
}