using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Scripts.Ui
{
    public interface IWindowManager
    {
        TWindowType OpenWindow<TWindowType>(string prefabName = null) where TWindowType : class, IWindow;
        void CloseAllWindows();
        void PutOnTop<TWindowType>(TWindowType newWindow) where TWindowType : IWindow;
        IWindow GetTopmostWindow();
    }

    public class WindowManager : IWindowManager
    {
        private readonly IWindowFactory _windowFactory;
        private LinkedList<IWindow> _openedWindows = new();

        public WindowManager(IWindowFactory windowFactory)
        {
            _windowFactory = windowFactory;
        }
        
        public TWindowType OpenWindow<TWindowType>(string prefabName = null) where TWindowType : class, IWindow
        {
            var existingWindow = GetOpenedWindow<TWindowType>(prefabName);
            if (existingWindow != null)
            {
                PutOnTop(existingWindow);
                return existingWindow as TWindowType;
            }

            var newWindow = _windowFactory.CreateWindowByType<TWindowType>(prefabName);
            newWindow.Closed += OnWindowClosed;
            PutOnTop(newWindow);
            return newWindow;
        }

        private void OnWindowClosed(IWindow window)
        {
            window.Closed -= OnWindowClosed;
            _openedWindows.Remove(window);
        }

        public void CloseAllWindows()
        {
            foreach (var window in new LinkedList<IWindow>(_openedWindows))
            {
                window.Close();
            }
            _openedWindows.Clear();
        }

        public void PutOnTop<TWindowType>(TWindowType newWindow) where TWindowType : IWindow
        {
            _openedWindows.Remove(newWindow);
            _openedWindows.AddLast(newWindow);
            newWindow.DrawOnTop();
        }

        [CanBeNull]
        public IWindow GetTopmostWindow()
        {
            return _openedWindows.LastOrDefault();
        }

        private IWindow GetOpenedWindow<TWindowType>(string prefabName = null) where TWindowType : IWindow
        {
            if (prefabName != null)
            {
                return _openedWindows.FirstOrDefault(w => w.PrefabName == prefabName);
            }
            return _openedWindows.FirstOrDefault(w => w is TWindowType);
        }
    }
}