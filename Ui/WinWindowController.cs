using System;
using Core;
using Core.Configs;
using Scripts.Core;
using Scripts.Ui.Windows;
using VContainer.Unity;

namespace Scripts.Ui
{
    public class WinWindowController : IDisposable, IInitializable
    {
        private ScoreController _scoreController;
        private readonly IWindowManager _windowManager;
        private readonly MatchStatistics _statistics;

        public WinWindowController(ScoreController scoreController, IWindowManager windowManager, MatchStatistics statistics)
        {
            _scoreController = scoreController;
            _windowManager = windowManager;
            _statistics = statistics;

            _scoreController.OnWin += ShowVictoryWindow;
        }

        public void Dispose()
        {
            _scoreController.OnWin -= ShowVictoryWindow;
        }

        private void ShowVictoryWindow(IParticipantDescription winner)
        {
            _scoreController.OnWin -= ShowVictoryWindow;
            _windowManager.CloseAllWindows();
            var window = _windowManager.OpenWindow<WinWindow>();

            var leftWin = winner == _statistics.StatsLeftParticipant.Participant.Desc;
            window.Draw(_statistics.StatsLeftParticipant, _statistics.StatsRightParticipant, leftWin);
        }

        public void Initialize()
        {
        }
    }
}