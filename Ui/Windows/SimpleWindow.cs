using UnityEngine;

namespace Scripts.Ui.Windows
{
    /// <summary>
    /// Для тех окон, которые в префабе вообще не имеют скрипта окна
    /// </summary>
    public class SimpleWindow : WindowBase
    {
        public RectTransform Transform => transform as RectTransform;
    }
}