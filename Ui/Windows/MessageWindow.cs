using TMPro;
using UnityEngine;

namespace Scripts.Ui.Windows
{
    public class MessageWindow : WindowBase
    {
        [SerializeField] private TextMeshProUGUI _text;

        public string Message
        {
            get => _text.text;
            set => _text.text = value;
        }
    }
}