using System;
using Scripts.Services;
using SdkIntegrations;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

namespace Scripts.Ui.Windows
{
    public class PauseWindow : WindowBase
    {
        [SerializeField] private Slider _musicSlider;
        [SerializeField] private Slider _sfxSlider;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _menuButton;
        
        [Inject] private readonly IMainMenuLoaderService _mainMenuLoaderService;
        [Inject] private readonly IMatchLoaderService _matchLoaderService;
        [Inject] private readonly IAudioService _audioService;
        [Inject] private readonly ISdk _sdk;

        private void Start()
        {
            _musicSlider.value = _audioService.MusicVolume;
            _sfxSlider.value = _audioService.SfxVolume;
            _musicSlider.onValueChanged.AddListener(v => _audioService.MusicVolume = v);
            _sfxSlider.onValueChanged.AddListener(v => _audioService.SfxVolume = v);
            
            _restartButton.onClick.AddListener(() => _matchLoaderService.RestartCurrentMatch());
            _menuButton.onClick.AddListener(() => _mainMenuLoaderService.LoadMainMenu());
            
            _sdk.GameplayStop();
        }

        private void OnDestroy()
        {
            _sdk.GameplayStart();
        }
    }
}