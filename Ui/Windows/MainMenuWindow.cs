using Scripts.Services;
using System;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

namespace Scripts.Ui.Windows
{
    public class MainMenuWindow : WindowBase
    {
        [Inject] private readonly IWindowManager _windowManager;
        [Inject] private readonly IMatchLoaderService _matchLoaderService;
        [Inject] private readonly IAdsRewardService _adsRewardService;

        [SerializeField] private Button _buttonPvp;
        [SerializeField] private Button _buttonPve;
        [SerializeField] private Button _buttonTraining;
        [SerializeField] private Button _addCoins;
        [SerializeField] private Button _buttonStore;
        [SerializeField] private Button _OnlinePvp;
        [SerializeField] private GameObject _notification;


        private FreeCashWindow _freeCashWindow; 

        private void AdRewardService_OnTimerEnded()
        {
            _notification.SetActive(true);
        }

        private void Start()
        {
            _buttonPvp.onClick.AddListener(OnPvpClick);
            _buttonPve.onClick.AddListener(OnPveClick);
            _buttonTraining.onClick.AddListener(OnTrainingClick);
            _addCoins.onClick.AddListener(OnAddCoins);
            _adsRewardService.OnTimerEnded += AdRewardService_OnTimerEnded;
            _buttonStore.onClick.AddListener(OnStoreClick);
            _OnlinePvp.onClick.AddListener(OnlinePvpClick);

        }

        private void OnDisable()
        {
            _adsRewardService.OnTimerEnded -= AdRewardService_OnTimerEnded;
        }


        private void OnlinePvpClick()
        {
            _windowManager.OpenWindow<MatchmakingWindow>();
        }

        private void OnAddCoins()
        {
            _freeCashWindow = _windowManager.OpenWindow<FreeCashWindow>();
            _freeCashWindow.OnTimerStarted += FreeCashWindow_OnTimerStarted;
        }

        private void OnStoreClick()
        {
            _windowManager.OpenWindow<StoreWindow>();
        }

        private void FreeCashWindow_OnTimerStarted()
        {
            _freeCashWindow.OnTimerStarted -= FreeCashWindow_OnTimerStarted;
            _notification.SetActive(false);
        }

        private void OnPveClick()
        {
            _windowManager.OpenWindow<ChooseAiEnemyWindow>();
            Close();
        }

        private void OnTrainingClick()
        {
            _windowManager.OpenWindow<TrainingWindow>();
        }

        private void OnPvpClick()
        {
            _windowManager.OpenWindow<PvpAimChoiseWindow>();
        }

    }
}