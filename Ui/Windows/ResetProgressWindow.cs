using Scripts.Services;
using SdkIntegrations;
using UnityEngine;
using UnityEngine.Localization.Components;
using UnityEngine.UI;
using VContainer;

namespace Scripts.Ui.Windows
{
    public class ResetProgressWindow : WindowBase
    {
        [Inject] private readonly IMainMenuLoaderService _mainMenuLoader;
        [Inject] private readonly ISdk _sdk;
        
        [SerializeField] private Button _okButton;
        [SerializeField] private LocalizeStringEvent _okButtonLocalizeEvent;

        private const int OkPressesRequired = 5;
        private int _okPressesLeft = OkPressesRequired;

        private void Start()
        {
            _okButton.onClick.AddListener(OkClicked);
            
            RefreshString();
        }

        private void OkClicked()
        {
            _okPressesLeft--;

            RefreshString();
            
            if (_okPressesLeft <= 0)
            {
                _sdk.ResetSaveProgress();
                _mainMenuLoader.LoadMainMenu();
            }
        }

        private void RefreshString()
        {
            _okButtonLocalizeEvent.StringReference.Arguments = new object[] { _okPressesLeft };
            _okButtonLocalizeEvent.RefreshString();
        }
    }
}