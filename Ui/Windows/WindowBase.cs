using System;
using UnityEngine;

namespace Scripts.Ui.Windows
{
    public abstract class WindowBase : MonoBehaviour, IWindow
    {
        public virtual void OnClosing() { }
        public virtual void OnClosed() { }
        public event Action<IWindow> Closed;
        public bool IsClosing { get; private set; }

        public string PrefabName { get; private set; }

        protected void Awake()
        {
            PrefabName = name.Substring(0, name.IndexOf("(Clone)", StringComparison.Ordinal));
        }
        public void DrawOnTop()
        {
            transform.SetAsLastSibling();
        }

        public void Close()
        {
            if (IsClosing) return;
            IsClosing = true;
            OnClosing();
            Destroy(gameObject);
            OnClosed();
            Closed?.Invoke(this);
        }
    }
}