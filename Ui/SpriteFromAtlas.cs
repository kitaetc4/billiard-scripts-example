using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

[ExecuteInEditMode]
public class SpriteFromAtlas : MonoBehaviour
{
    [SerializeField] SpriteAtlas atlas;
    [SerializeField] string spriteName; // todo тут можно вывести автоматическую менюшку

    private void Start()
    {
        RefreshSprite();
    }

    private void OnGUI()
    {
        RefreshSprite();
    }

    private void RefreshSprite()
    {
        GetComponent<Image>().sprite = atlas.GetSprite(spriteName);
    }
}