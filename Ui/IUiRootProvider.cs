using UnityEngine;

namespace Core
{
    public interface IUiRootProvider
    {
        RectTransform GetWindowsRoot();
    }
}