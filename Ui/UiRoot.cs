using UnityEngine;

namespace Core
{
    public class UiRoot : MonoBehaviour, IUiRootProvider
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        [SerializeField] private RectTransform _windowRoot;

        public RectTransform GetWindowsRoot() => _windowRoot;
    }
}