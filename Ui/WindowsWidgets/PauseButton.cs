using Scripts.Ui.Windows;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

namespace Scripts.Ui.WindowsWidgets
{
    public class PauseButton : MonoBehaviour
    {
        [Inject] private readonly IWindowManager _windowManager;

        private void Start()
        {
            var button = GetComponent<Button>();
            button.onClick.AddListener(TogglePause);
        }

        private void TogglePause()
        {
            var currentTopmostWindow = _windowManager.GetTopmostWindow();
            if (currentTopmostWindow != null 
                && currentTopmostWindow.GetType() == typeof(PauseWindow))
            {
                currentTopmostWindow.Close();
            }
            else
            {
                _windowManager.OpenWindow<PauseWindow>();
            }
        }
    }
}