using UnityEngine;
using UnityEngine.UI;

namespace Scripts.Ui.WindowsWidgets
{
    public class CloseWindowButton : MonoBehaviour
    {
        private void Start()
        {
            var button = GetComponent<Button>();

            button.onClick.AddListener(Close);
        }

        private void Close()
        {
            var window = GetComponent<IWindow>();
            if (window == null)
            {
                window = GetComponentInParent<IWindow>();
            }

            if (window != null)
            {
                window.Close();
            }
        }
    }
}