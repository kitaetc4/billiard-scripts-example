﻿using Scripts.Core.Participants;
using TMPro;
using UnityEngine;
using VContainer;
using Core.Utils;

namespace Scripts.Ui.WindowsWidgets
{
    public class MatchRewardView : MonoBehaviour
    {
        [Inject] private readonly IMatchModel _matchModel;
        [Inject] private readonly IWinRewardController _winRewardController;

        private void Start()
        {
            var text = GetComponent<TextMeshProUGUI>();
            text.text = ConvertNumbers.ScoreShow(_matchModel.WinReward.Amount);
        }
    }
}