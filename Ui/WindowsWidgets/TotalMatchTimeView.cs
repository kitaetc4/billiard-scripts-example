using System;
using Scripts.Core;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using VContainer;

namespace Scripts.Ui.WindowsWidgets
{
    public class TotalMatchTimeView : MonoBehaviour
    {
        [Inject] private readonly MatchTimer _matchTimer;
        [SerializeField] private LocalizedString _localizedString;
        [SerializeField] private TextMeshProUGUI _text;

        private string MatchTimeString => TimeSpan.FromSeconds(_matchTimer.MatchTime).ToString("mm':'ss");

        private void Start()
        {
            _text = GetComponent<TextMeshProUGUI>();
            _localizedString.Arguments = new object[] { 0 };
            _localizedString.StringChanged += UpdateText;
            Refresh();
        }

        private void OnDestroy()
        {
            _localizedString.StringChanged -= UpdateText;
        }

        private void Refresh()
        {
            _localizedString.Arguments[0] = MatchTimeString;
            _localizedString.RefreshString();
        }

        private void UpdateText(string value)
        {
            _text.text = value;
        }
    }
}