﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Ui.WindowsWidgets
{
    public class ImageLoadPlayerPhoto
    {
        public static async UniTask<Sprite> LoadAsync(string url, Sprite sprite)
        {
#if UNITY_2020_1_OR_NEWER
            using UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(url);
            await webRequest.SendWebRequest();

            if (webRequest.result is not UnityWebRequest.Result.ConnectionError 
                and not UnityWebRequest.Result.DataProcessingError)
            {
                DownloadHandlerTexture handlerTexture = webRequest.downloadHandler as DownloadHandlerTexture;

                if (sprite)
                {
                    if (handlerTexture.isDone)
                        sprite = Sprite.Create((Texture2D)handlerTexture.texture,
                            new Rect(0, 0, handlerTexture.texture.width, handlerTexture.texture.height), Vector2.zero);
                    return sprite;
                }
            }
            return null;
#endif
        }

    }
}