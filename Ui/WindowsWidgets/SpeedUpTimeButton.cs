﻿using Core;
using States;
using UnityEngine;
using UnityEngine.EventSystems;
using VContainer;

namespace Scripts.Ui.WindowsWidgets
{
    public class SpeedUpTimeButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [Inject] private readonly IGameStateProvider _gameState;
        [Inject] private readonly ICurrentParticipantProvider _currentParticipant;
        
        public bool IsPressed { get; private set; }

        private void Awake()
        {
            _gameState.StateChanged += StateChanged;
        }

        private void StateChanged(GameStateEnum newState)
        {
            var botsTurn = _currentParticipant.CurrentParticipant.IsBot;
            var needToEnable = newState == GameStateEnum.BallsRollingState || botsTurn;
            gameObject.SetActive(needToEnable);
        }

        private void EnableTimeScale()
        {
            IsPressed = true;
        }

        private void ResetTimeScale()
        {
            IsPressed = false;
        }

        public void OnPointerDown(PointerEventData data)
        {
            EnableTimeScale();
        }

        public void OnPointerUp(PointerEventData data)
        {
            ResetTimeScale();
        }

        private void OnDisable()
        {
            ResetTimeScale();
        }
    }
}