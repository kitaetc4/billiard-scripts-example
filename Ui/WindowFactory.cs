using System;
using System.Collections.Generic;
using Core;
using Scripts.Ui.Windows;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Scripts.Ui
{
    public class WindowFactory : IWindowFactory
    {
        private readonly IUiRootProvider _uiRoot;
        private readonly Dictionary<string, RectTransform> _windowsNames;
        private readonly Dictionary<Type, RectTransform> _windowsTypes = new();
        
        private IObjectResolver _diContainer;

        public WindowFactory(IUiRootProvider rootProvider, Dictionary<string, RectTransform> windowsNamesStorage)
        {
            _uiRoot = rootProvider;
            _windowsNames = windowsNamesStorage;
            foreach (var w in _windowsNames.Values)
            {
                if (w.TryGetComponent<IWindow>(out var windowComponent))
                {
                    _windowsTypes[windowComponent.GetType()] = w;
                }
            }
        }
        
        public TWindowType CreateWindowByType<TWindowType>(string prefabName = null) where TWindowType : IWindow
        {
            if (prefabName != null && _windowsNames.TryGetValue(prefabName, out var namedWindowPrefab))
            {
                var instance = InstantiateWindow(namedWindowPrefab);
                return instance.GetComponent<TWindowType>();
            }
            if (_windowsTypes.TryGetValue(typeof(TWindowType), out var windowPrefab))
            {
                var instance = InstantiateWindow(windowPrefab);
                return instance.GetComponent<TWindowType>();
            }
            else
            {
                throw new KeyNotFoundException($"Нет такого {typeof(TWindowType).GetType().Name} {prefabName} окна в Resoudces/Windows/");
            }
        }

        public IWindow CreateWindowByName(string prefabName)
        {
            return CreateWindowByType<WindowBase>(prefabName);
        }
        
        private WindowBase InstantiateWindow(RectTransform windowPrefab)
        {
            var root = _uiRoot.GetWindowsRoot();
            var instance = _diContainer.Instantiate(windowPrefab, root);
            WindowBase windowBase = default;
            if (!instance.TryGetComponent<WindowBase>(out windowBase))
            {
                windowBase = instance.gameObject.AddComponent<SimpleWindow>();
            }
            _diContainer.Inject(windowBase);
            foreach (var childWidget in windowBase.GetComponentsInChildren<MonoBehaviour>(includeInactive: true))
            {
                _diContainer.Inject(childWidget);
            }
            return windowBase;
        }

        public void SetContainer(IObjectResolver container)
        {
            _diContainer = container;
        }

        public IObjectResolver GetContainer()
        {
            return _diContainer;
        }
    }
}