using System.Collections.Generic;
using System.Linq;
using Core;
using Core.Utils;
using UnityEngine;
using VContainer;

namespace Scripts.Ui
{
    public static class UiContainerBuilderExtensions
    {
        public static void RegisterWindowServices(this IContainerBuilder builder, Lifetime lifetime)
        {
            builder.Register<IWindowManager, WindowManager>(lifetime);
            builder.Register<IWindowFactory, WindowFactory>(lifetime);
            builder.RegisterBuildCallback(container => {           
                var windowFactory = container.Resolve<IWindowFactory>() as WindowFactory;
                windowFactory.SetContainer(container);
            });
            
            builder.RegisterInHierarchyOrCreateFromResources<UiRoot>("Windows/WindowsCanvas").AsSelfAndInterfaces();
            
            // RegisterWindows
            RectTransform[] prefabs = Resources.LoadAll<RectTransform>("Windows");
            Dictionary<string, RectTransform> windowsStorage = prefabs.ToDictionary(w => w.name);
            builder.RegisterInstance<Dictionary<string, RectTransform>>(windowsStorage);
        } 
    }
}