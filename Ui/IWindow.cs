using System;

namespace Scripts.Ui
{
    public interface IWindow
    {
        event Action<IWindow> Closed;
        void Close();
        void DrawOnTop();
        string PrefabName { get; }
    }
}