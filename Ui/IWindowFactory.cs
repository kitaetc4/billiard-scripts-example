namespace Scripts.Ui
{
    public interface IWindowFactory
    {
        TWindowType CreateWindowByType<TWindowType>(string prefabName = null) where TWindowType : IWindow;
        IWindow CreateWindowByName(string prefabName);
    }
}