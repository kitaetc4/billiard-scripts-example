using Scripts.Scriptables;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core.BilliardMath
{
    public class TrajectoryCalculator : IInitializable
    {
        [Inject] private readonly IBallsProvider _ballsProvider;
        [Inject] private readonly GlobalConfig _config;

        private float _ballRadius;

        private const float OtherBallFrictionCorrection = 0.0065f;
        private const float StrickenBallFrictionCorrection = -0.015f;

        public void Initialize()
        {
        }

        public ImpactInfo GetFirstImpact(Vector3 ballPosition, Vector3 strikeDirection)
        {
            Physics.SphereCast(ballPosition, _config.BallRadius, strikeDirection, out RaycastHit hit);
            if (hit.transform == null) return default;
            var ballPosOnCollision = hit.point + _config.BallRadius * hit.normal;
            var didHitOtherBall = hit.transform.gameObject.layer == LayerMask.NameToLayer("Ball");
            var otherObjectPos = hit.transform.position;
            
            var impact = new ImpactInfo
            {
                DidHitOtherBall = didHitOtherBall,
                OtherObjectPos = otherObjectPos,
                BallPosOnCollision = ballPosOnCollision,
            };
            
            if (didHitOtherBall)
            {
                var ballPosOnCollisionCorrected1 = CorrectPosOnCollisionForOtherBall(strikeDirection, ballPosOnCollision);
                impact.OtherBallRollDirection = otherObjectPos - ballPosOnCollisionCorrected1;
                
                var ballPosOnCollisionCorrected2 = CorrectPosOnCollisionForStrikeBall(strikeDirection, ballPosOnCollision);
                var normalCorrected = (ballPosOnCollisionCorrected2 - otherObjectPos).normalized;
                Vector3 cross = Vector3.Cross(normalCorrected, (ballPosOnCollisionCorrected2 - ballPosition));
                impact.ReflectDir = Vector3.Cross(cross, normalCorrected);
            }
            else 
            {
                impact.ReflectDir = Vector3.Reflect(strikeDirection, hit.normal);
            }
            impact.Angle = Vector3.Angle(hit.normal, ballPosition - ballPosOnCollision);

            return impact;
        }

        private static Vector3 CorrectPosOnCollisionForOtherBall(Vector3 strikeDirection, Vector3 ballPosOnCollision)
        {
            return ballPosOnCollision - strikeDirection.normalized * OtherBallFrictionCorrection;
        }

        private static Vector3 CorrectPosOnCollisionForStrikeBall(Vector3 strikeDirection, Vector3 ballPosOnCollision)
        {
            return ballPosOnCollision - strikeDirection.normalized * StrickenBallFrictionCorrection;
        }

        // todo скорее всего не нужен и на этапе его вызова он возвращает true
        public bool IsStrikePossibleByAngle(Vector3 ballToStrikePos, Vector3 ballToHitPos, Vector3 positionToScore)
        {
            var directionToScore = (positionToScore - ballToHitPos).normalized;
            var collisionPos = CalculateCollisionPosToScore(ballToStrikePos, ballToHitPos, positionToScore);
            var strikeDir = (collisionPos - ballToStrikePos).normalized;

            var angleCos = Vector3.Dot(directionToScore, strikeDir);
            var angle = Mathf.Acos(angleCos) * Mathf.Rad2Deg;
            angle = Vector3.Angle(strikeDir, directionToScore);
            
            return angle < 80;
        }

        public Vector3 CalculateCollisionPosToScore(Vector3 ballToStrikePos, Vector3 ballToHitPos, Vector3 positionToScore)
        {
            var directionToScore = (positionToScore - ballToHitPos).normalized;
            var posOnCollision = ballToHitPos + -directionToScore * _ballRadius * 2f;
            var strikeDir = posOnCollision - ballToStrikePos;
            var correctedPosCollision = posOnCollision - strikeDir.normalized * +OtherBallFrictionCorrection;
            var correctedTargetPos = (correctedPosCollision - ballToHitPos).normalized * _ballRadius * 2f + ballToHitPos;
            return (posOnCollision + correctedTargetPos) / 2f;
            // todo перевести на таблицу известных углов отскока
        }

        public Vector3 GetLastPoint(Vector3 startRayPosition, Vector3 direction, float rayLength = 100f)
        {
            Ray ray = new Ray(startRayPosition, direction);
            if (Physics.Raycast(ray, out RaycastHit hitinfo, rayLength))
            {
                return hitinfo.point;
            }
            else return ray.GetPoint(rayLength);
        }
    }
}

public struct ImpactInfo
{
    public bool DidHitOtherBall;
    public Vector3 OtherObjectPos;
    public Vector3 BallPosOnCollision;
    public Vector3 OtherBallRollDirection;
    public Vector3 ReflectDir;
    public float Angle;
}