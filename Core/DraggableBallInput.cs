using Core.Rules;
using Core.Utils;
using InputSystem;
using UnityEngine;
using VContainer;

namespace Core
{
    public class DraggableBallInput : MonoBehaviour
    {
        [SerializeField] private float _speedMultiplier = 1f;
        [Inject] private IRulesProvider _rulesProvider;
        [Inject] private InputManager _inputManager;
        private Vector3 _worldOffset;
        private float _zCoord;
        private float _startYCoord;

        private void OnMouseDown()
        {
            _zCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
            _startYCoord = gameObject.transform.position.y;
            _worldOffset = gameObject.transform.position - GetMouseAsWorldPoint();
        }

        private Vector3 GetMouseAsWorldPoint()
        {
            // Pixel coordinates of mouse (x,y)
            Vector3 mousePoint = Input.mousePosition;

            // z coordinate of game object on screen
            mousePoint.z = _zCoord;

            // Convert it to world points
            return Camera.main.ScreenToWorldPoint(mousePoint);
        }

        public void OnMouseDrag()
        {
            if (!_rulesProvider.IsAllowedToMoveBallInHome()) return;
            var pos = GetMouseAsWorldPoint() + _worldOffset;
            pos.y = _startYCoord;
            var delta = (pos - transform.position) * _speedMultiplier;
            // if (delta.magnitude > _speedMultiplier * Time.deltaTime) delta = delta.normalized * _speedMultiplier * Time.deltaTime;
            
            _inputManager.SetDragBallInput(delta.ToXz());
        }
    }
}