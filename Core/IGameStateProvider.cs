using System;
using States;

namespace Core
{
    public interface IGameStateProvider
    {
        public event Action<GameStateEnum> StateChanged;
        public GameStateEnum GetCurrentState();
    }
}