using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using States;
using UnityEngine;
using VContainer.Unity;

namespace Core
{
    public class GameStateEngine : IAsyncStartable, IDisposable
    {
        public GameStateEnum FirstState { get; set; } = GameStateEnum.StrikeBallState; 
        
        private readonly IReadOnlyList<IGameState> _gameStates;
        private readonly GameStateProvider _stateProvider;
        private readonly CancellationTokenSource _lifeTimeCancellationTokenSource = new CancellationTokenSource();

        private IGameState this[GameStateEnum gameState] => _gameStates[(int) gameState];

        public GameStateEngine(IReadOnlyList<IGameState> gameStates, GameStateProvider stateProvider)
        {
            _gameStates = gameStates;
            _stateProvider = stateProvider;
        }

        public async UniTask StartAsync(CancellationToken cancellationToken)
        {
            var token = cancellationToken == default ? _lifeTimeCancellationTokenSource.Token : cancellationToken;

            await UniTask.Yield(PlayerLoopTiming.PostLateUpdate);
            RunStateEngine(token);
        }

        private void RunStateEngine(CancellationToken token)
        {
            var nextState = FirstState;
            _stateProvider.SetCurrentState(nextState);
            Debug.Log($"{nameof(GameStateEngine)}: Initial game state: {nextState.ToString()}");
            UniTaskAsyncEnumerable.EveryUpdate()
                .SubscribeAwait(async _ =>
                {
                    var activeState = this[nextState];
                    await activeState.OnStateBegan(token);
                    nextState = activeState.OnStateEnded();
                    _stateProvider.SetCurrentState(nextState);
                    Debug.Log($"{nameof(GameStateEngine)}: New state: {nextState.ToString()}, prev state: {activeState.GetType().Name}");
                }, token);
        }
        
        public void Dispose()
        {
            _lifeTimeCancellationTokenSource?.Cancel();
            _lifeTimeCancellationTokenSource?.Dispose();
        }
    }
}