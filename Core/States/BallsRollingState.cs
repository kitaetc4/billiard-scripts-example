using System.Threading;
using Core;
using Cysharp.Threading.Tasks;
using VContainer;

namespace States
{
    public class BallsRollingState : IGameState
    {
        [Inject] private readonly BallSettleController _settleController;
        [Inject] private readonly TurnController _turnController;
        [Inject] private readonly GoalController _goalController;
        [Inject] private readonly BallFoulController _ballFoulController;
        [Inject] private readonly BallSelectorController _ballSelectorController;
        [Inject] private readonly ScoreController _scoreController;
        private const int DelayBeforeBallsRoll = 100;
        private bool _anyBallWasGoaledOnThisStrike;
        private bool _wasFoul;

        public async UniTask OnStateBegan(CancellationToken token)
        {
            await UniTask.Delay(DelayBeforeBallsRoll, cancellationToken: token);

            _ballFoulController.ResetFoul();
            _anyBallWasGoaledOnThisStrike = false;
            _wasFoul = false;
            _goalController.OnAnyBallGoaled += AnyBallGoaled;
            _ballFoulController.OnAnyBallGoaledWithFoul += AnyBallGoaledWithFoul;
            _settleController.Unsettle();

            await UniTask.WaitUntil(() => _settleController.Settled || _scoreController.GameEnded, 
                cancellationToken: token);
        }

        private void AnyBallGoaled(Ball ball)
        {
            _anyBallWasGoaledOnThisStrike = true;
        }

        private void AnyBallGoaledWithFoul(Ball ball)
        {
            _wasFoul = true;
        }

        public GameStateEnum OnStateEnded()
        {
            _goalController.OnAnyBallGoaled -= AnyBallGoaled;
            _ballFoulController.OnAnyBallGoaledWithFoul -= AnyBallGoaledWithFoul;

            if (!_anyBallWasGoaledOnThisStrike && !_wasFoul)
            {
                _ballSelectorController.Unselect();
                _turnController.SwitchToNextPlayer();
                return GameStateEnum.SelectBallState;
            }
            else if (_wasFoul)
            {
                _ballSelectorController.Unselect();
                _turnController.SwitchToNextPlayer();
                return GameStateEnum.FoulPickBallState;
            }
            else if (_scoreController.GameEnded)
            {
                return GameStateEnum.WinState;
            }
            else
            {
                return GameStateEnum.SelectBallState;
            }
        }
    }
}