﻿using UnityEngine;
using Cysharp.Threading.Tasks;
using System.Threading;
using Core;
using System;
using Scripts.Services;
using Scripts.Ui.Windows;
using Scripts.Ui;
using VContainer;

namespace States
{
    public class FoulPickBallState : IGameState
    {
        private const string ChooseFoulBallKey = "ChooseFoulBall";
        private const string SelectBallWindowName = "FoulMessage";

        [Inject] private readonly IWindowManager _windowManager;
        [Inject] private readonly ICurrentParticipantProvider _currentParticipant;
        [Inject] private readonly BallSelectorController _ballSelectorController;
        [Inject] private readonly BallsProvider _ballsProvider;
        [Inject] private readonly ScoreController _scoreController;
        
        private MessageWindow _foulWindow;

        public event Action<Ball> OnFoulRemoved;

        public async UniTask OnStateBegan(CancellationToken token)
        {
            if (_scoreController.GameEnded) return;
            
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                var mat = ball.gameObject.GetComponent<MeshRenderer>().material;
                mat.SetFloat("_Boolean", 1.0f);
            }
            
            _foulWindow = _windowManager.OpenWindow<MessageWindow>(SelectBallWindowName);
            _currentParticipant.CurrentParticipant.Desc.Name.StringChanged += RefreshString;

            await UniTask.WaitUntil(() => _ballSelectorController.AnyBallSelected, cancellationToken: token);

            Ball selectedball = _ballSelectorController.SelectedBall;
            _ballsProvider.OnGoaled(selectedball);
            GameObject.Destroy(selectedball.gameObject);
            _ballSelectorController.Unselect();
            var player = _currentParticipant.CurrentParticipant.Desc;
            _scoreController.GetScoreForParticipant(player).AddBall(null);
        }

        private void RefreshString(string value)
        {
            if (_foulWindow != null)
            {
                LocalizationService.GetTranslatedStringAsync(ChooseFoulBallKey, str =>
                {
                    var selectBallMessage = value + ", " + str;
                    _foulWindow.Message = selectBallMessage;
                });
            }
        }

        public GameStateEnum OnStateEnded()
        {
            if (_scoreController.GameEnded) return GameStateEnum.WinState;
            _currentParticipant.CurrentParticipant.Desc.Name.StringChanged -= RefreshString;
            
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                var mat = ball.gameObject.GetComponent<MeshRenderer>().material;
                mat.SetFloat("_Boolean", 0.0f);
            }
            _foulWindow.Close();
            _foulWindow = null;
            return GameStateEnum.SelectBallState;
        }
    }
}