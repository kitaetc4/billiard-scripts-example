using System.Threading;
using Core;
using Core.Rules;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace States
{
    public class StrikeBallState : IGameState
    {
        private readonly IRulesProvider _rulesProvider;
        private readonly HomeZoneMovableBall _homeZoneMovableBall;
        private readonly StrikeController _strikeController;
        private readonly BallSelectorController _selectorController;
        private readonly ICurrentParticipantProvider _currentParticipant;
        private readonly IBallsProvider _ballsProvider;
        private readonly ScoreController _scoreController;

        private bool _wasStricken = false;

        public StrikeBallState(IRulesProvider rulesProvider,
            HomeZoneMovableBall homeZoneMovableBall,
            StrikeController strikeController,
            BallSelectorController selectorController,
            ICurrentParticipantProvider currentParticipant,
            ScoreController scoreController,
            IBallsProvider ballsProvider)
        {
            _rulesProvider = rulesProvider;
            _homeZoneMovableBall = homeZoneMovableBall;
            _strikeController = strikeController;
            _selectorController = selectorController;
            _currentParticipant = currentParticipant;
            _ballsProvider = ballsProvider;
            _scoreController = scoreController;
        }

        public async UniTask OnStateBegan(CancellationToken token)
        {
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                ball.BeforeStrikePosition = ball.transform.position;
                ball.CollidedWithOtherBall = false;
            }

            _wasStricken = false;
            _currentParticipant.CurrentParticipant.OnStrike += StrikeBall;
            _currentParticipant.CurrentParticipant.OnBallMoveInHomeZone += MoveBallInHomeZone;

            await UniTask.WaitUntil(() => _wasStricken || _scoreController.GameEnded, cancellationToken: token);
        }

        private void MoveBallInHomeZone(Vector2 movement)
        {
            if (!_rulesProvider.IsAllowedToMoveBallInHome()) return;
            _homeZoneMovableBall.MoveInHomeZone(movement);
        }

        private void StrikeBall(float power)
        {
            _strikeController.SetBall(_selectorController.SelectedBall);
            _strikeController.Strike(power);
            _selectorController.Unselect();
            _wasStricken = true;

            _currentParticipant.CurrentParticipant.OnStrike -= StrikeBall;
            _currentParticipant.CurrentParticipant.OnBallMoveInHomeZone -= MoveBallInHomeZone;
        }

        public GameStateEnum OnStateEnded()
        {
            if (_scoreController.GameEnded) return GameStateEnum.WinState;
            return GameStateEnum.BallsRollingState;
        }
    }
}