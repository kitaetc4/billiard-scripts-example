namespace States
{
    public enum GameStateEnum
    {
        StrikeBallState,
        BallsRollingState,
        SelectBallState,
        FoulPickBallState,
        WinState,
    }
}