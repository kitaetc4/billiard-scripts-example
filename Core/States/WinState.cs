﻿using System.Threading;
using Cysharp.Threading.Tasks;
using States;

namespace Scripts.Core.States
{
    public class WinState : IGameState
    {
        public UniTask OnStateBegan(CancellationToken token)
        {
            return default;
        }

        public GameStateEnum OnStateEnded()
        {
            return GameStateEnum.WinState;
        }
    }
}