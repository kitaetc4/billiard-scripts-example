using System.Threading;
using Core;
using Cysharp.Threading.Tasks;
using Scripts.Ui;
using Scripts.Ui.Windows;

namespace States
{
    public class SelectBallState : IGameState
    {
        private MessageWindow _selectBallWindow;
        private const string SelectBallWindowName = "ChooseBallMessage";
        private readonly BallSelectorController _ballSelectorController;
        private readonly IWindowManager _windowManager;
        private readonly ICurrentParticipantProvider _currentParticipant;
        private readonly ScoreController _scoreController;

        private Ball _selectedBall;
        private bool _wasSelected;

        public SelectBallState(BallSelectorController ballSelectorController, IWindowManager windowManager, 
            ScoreController scoreController,
            ICurrentParticipantProvider currentParticipant)
        {
            _ballSelectorController = ballSelectorController;
            _windowManager = windowManager;
            _currentParticipant = currentParticipant;
            _scoreController = scoreController;
        }
        
        public async UniTask OnStateBegan(CancellationToken token)
        {
            if (_scoreController.GameEnded) return;
            _selectBallWindow = _windowManager.OpenWindow<MessageWindow>(SelectBallWindowName);
            _currentParticipant.CurrentParticipant.Desc.Name.StringChanged += RefreshString;
            await UniTask.WaitUntil(() => _ballSelectorController.AnyBallSelected, cancellationToken: token);
        }

        private void RefreshString(string value)
        {
            if (_selectBallWindow != null)
            {
                var selectBallMessage = value + ", " + _selectBallWindow.Message;
                _selectBallWindow.Message = selectBallMessage;
            }
        }

        public GameStateEnum OnStateEnded()
        {
            if (_scoreController.GameEnded) return GameStateEnum.WinState;
            _currentParticipant.CurrentParticipant.Desc.Name.StringChanged -= RefreshString;
            _selectBallWindow.Close();
            _selectBallWindow = null;
            return GameStateEnum.StrikeBallState;
        }
    }
}