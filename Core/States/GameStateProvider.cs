using System;
using States;

namespace Core
{
    public class GameStateProvider : IGameStateProvider
    {
        public event Action<GameStateEnum> StateChanged;
        private GameStateEnum _state;

        private bool _inited = false;
        
        public GameStateEnum GetCurrentState()
        {
            return _state;
        }

        public void SetCurrentState(GameStateEnum state)
        {
            if (_state == state && _inited) return;
            _inited = true;
            _state = state;
            StateChanged?.Invoke(state);
        }
    }
}