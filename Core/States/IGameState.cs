using System.Threading;
using Cysharp.Threading.Tasks;

namespace States
{
    public interface IGameState
    {
        UniTask OnStateBegan(CancellationToken token);
        GameStateEnum OnStateEnded();
    }
}