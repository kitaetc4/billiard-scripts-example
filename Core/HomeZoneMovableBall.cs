using Core;
using UnityEngine;
using VContainer;

public class HomeZoneMovableBall : MonoBehaviour
{
    [Inject] private HomeZoneController _hz;
    [SerializeField] private float _speed = 1f;
    [SerializeField, Range(0.1f, 1f)] private float _smoothness = 0.9f;

    public void MoveInHomeZone(Vector2 input)
    {
        Vector3 offset = new Vector3(input.x, 0, input.y) * Time.deltaTime * _speed;
        transform.Translate(offset * (1 - _smoothness));
        ClampPosInHomeZone();
    }

    public void ClampPosInHomeZone()
    {
        var pos = transform.position;
        if (pos.x < -_hz.MaxMinPosX) transform.position = new Vector3(-_hz.MaxMinPosX, pos.y, pos.z);
        if (pos.x > _hz.MaxMinPosX) transform.position = new Vector3(_hz.MaxMinPosX, pos.y, pos.z);
        if (pos.z > _hz.MaxPosZ) transform.position = new Vector3(pos.x, pos.y, _hz.MaxPosZ);
        if (pos.z < _hz.MinPosZ) transform.position = new Vector3(pos.x, pos.y, _hz.MinPosZ);
    }
}
