using Cinemachine;
using Core.Ai;
using Core.BilliardMath;
using Core.Configs;
using Core.Controllers;
using Core.Rules;
using Core.Utils;
using Core.View;
using InputSystem;
using Scripts;
using Scripts.Core;
using Scripts.Core.Participants;
using Scripts.Core.States;
using Scripts.InputSystem;
using Scripts.Scriptables;
using Scripts.Ui;
using Scripts.Ui.WindowsWidgets;
using States;
using Ui;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core
{
    /// <summary>
    /// Корень Di для сцены игры
    /// Всё, созданное здесь, существует до конца матча, потом удаляется
    /// </summary>
    public class GameplayScope : LifetimeScope
    {
        [SerializeField] private ApplicationScope _applicationScope;
        private IObjectResolver _baseContainer;
        
#if UNITY_EDITOR
        protected override void Awake()
        {
            // если в редакторе открыли сцену с GameplayScope, то досоздаём родительский ApplicationScope
            var parent = FindObjectOfType<ApplicationScope>();
            if (parent == null)
            {
                // _applicationScope.autoRun = false;
                parent = Instantiate(_applicationScope);
                // parent.autoRun = false;
                // parent.Build();
                // _applicationScope.autoRun = true;
            }
            base.Awake();
        }
#endif

        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(FindObjectsOfType<HoleAiConfig>());
            builder.RegisterComponentInHierarchy<BallShotDirection>();
            builder.RegisterComponentInHierarchy<HomeZoneMovableBall>();
            builder.RegisterComponentInHierarchy<CueController>();
            builder.RegisterComponentInHierarchy<DrawTrajectoryView>();
            builder.RegisterComponentInHierarchy<CamerasEnabler>();
            builder.RegisterComponentInHierarchy<SelectCameraButton>();
            builder.RegisterComponentInHierarchy<DraggableBallInput>();
            builder.RegisterComponentInHierarchy<CueUiPowerInput>();
            builder.RegisterComponentInHierarchy<VJHandler>();
            builder.RegisterComponentInHierarchy<SpinController>();
            builder.RegisterComponentInHierarchy<HomeZoneController>();
            builder.RegisterComponentInHierarchy<InfinityScroll>();
            builder.RegisterComponentInHierarchy<ParticipantPanelView>();
            builder.RegisterComponentInHierarchy<PauseButton>();
            builder.RegisterComponentInHierarchy<MatchRewardView>();
            builder.RegisterComponentInHierarchy<Tutorial>();
            builder.RegisterComponentInHierarchy<CinemachineTargetGroup>(); // CinemachineGroupBalls
            builder.RegisterComponentInHierarchy<CheatsController>();
            builder.RegisterComponentInHierarchy<SpeedUpTimeButton>();
            builder.RegisterComponentInHierarchy<PlayerAvatarView>();
            builder.RegisterComponentInHierarchy<PlayerNameView>();
            builder.RegisterComponentOnNewGameObject<CheatBallsTrajectoryDisplay>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<IBallsProvider, BallsProvider>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<CameraNonFollowGoaledBallsController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<InputManager>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<BallSettleController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<BallFoulController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<StrikeController>(Lifetime.Scoped);
            builder.Register<IRulesProvider, RussianFreePyramidRules>(Lifetime.Scoped);
            builder.Register<BallSelectorController>(Lifetime.Scoped);
            builder.Register<BallSelectorInput>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<PinchZoomInput>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<CameraController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<CameraPathRotationService>(Lifetime.Scoped);
            builder.Register<HoleSelectorService>(Lifetime.Scoped);
            builder.Register<TurnController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<CurrentParticipantProvider>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<GoalController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<PlayerMatchParticipant>(Lifetime.Scoped);
            builder.Register<BotMatchParticipant>(Lifetime.Scoped);
            builder.Register<TrajectoryCalculator>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<IBotBrain, HardBotBrain>(Lifetime.Scoped);
            builder.Register<IDirectionAroundBallInput, DirectionAroundBallInput>(Lifetime.Scoped).As<ITickable>();
            builder.Register<IDirectionPrecisionInput, DirectionPrecisionInput>(Lifetime.Scoped);
            builder.Register<IDirectionFromTopInput, DirectionFromTopInput>(Lifetime.Scoped).As<ITickable>();
            builder.Register<BallsEstimator>(Lifetime.Scoped);
            builder.Register<LookupTableManager>(Lifetime.Scoped);
            builder.Register<BotStrikePowerCalculator>(Lifetime.Scoped);
            builder.Register<ScoreController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<WinWindowController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<CameraPinchZoom>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<CamerasZoomer>(Lifetime.Scoped);
            builder.Register<WinRewardController>(Lifetime.Scoped).AsImplementedInterfaces();
            builder.Register<MatchStatistics>(Lifetime.Scoped);
            builder.Register<MatchTimer>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<BallFallFromTableController>(Lifetime.Scoped).AsSelfAndInterfaces();
            builder.Register<DynamicPhysicsTimestepController>(Lifetime.Scoped).AsSelfAndInterfaces();
            
            RegisterParticipants(builder);
            RegisterGameplayStates(builder);
            builder.RegisterEntryPoint<GameStateEngine>(Lifetime.Scoped).AsSelf();
            RegisterEditorTools(builder);
            builder.RegisterBuildCallback(OnBuild);
        }

        private void OnBuild(IObjectResolver container)
        {
            var windowManager = container.Resolve<IWindowFactory>() as WindowFactory;
            _baseContainer = windowManager.GetContainer();
            windowManager.SetContainer(container);
        }

        protected override void OnDestroy()
        {
            var windowManager = _baseContainer.Resolve<IWindowFactory>() as WindowFactory;
            windowManager.SetContainer(_baseContainer);
            base.OnDestroy();
        }

        private void RegisterEditorTools(IContainerBuilder builder)
        {
#if UNITY_EDITOR
            // builder.Register<BallRegionDebugDisplay>(Lifetime.Scoped).AsImplementedInterfaces();
            builder.RegisterComponentInHierarchy<BotDebugDisplay>();
#endif
            // builder.Register<TouchDebugger>(Lifetime.Scoped).AsSelfAndInterfaces();
        }

        private void RegisterParticipants(IContainerBuilder builder)
        {
            builder.Register<ParticipantsFactory>(Lifetime.Scoped);
            builder.RegisterFactory<IMatchParticipant[]>(container =>
            {
                return () => container.Resolve<ParticipantsFactory>().CreateParticipants(container);
            }, Lifetime.Scoped);
        }

        private static void RegisterGameplayStates(IContainerBuilder builder)
        {
            builder.Register<IGameState, StrikeBallState>(Lifetime.Scoped);
            builder.Register<IGameState, BallsRollingState>(Lifetime.Scoped);
            builder.Register<IGameState, SelectBallState>(Lifetime.Scoped);
            builder.Register<IGameState, FoulPickBallState>(Lifetime.Scoped);
            builder.Register<IGameState, WinState>(Lifetime.Scoped);
            builder.Register<GameStateProvider>(Lifetime.Scoped).AsSelfAndInterfaces();
        }
    }
}