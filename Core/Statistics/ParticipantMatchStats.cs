using Core;
using UnityEngine;

namespace Scripts.Core
{
    public class ParticipantMatchStats
    {
        public int Strikes { get; private set; }
        public int Goals => _scoreController.GetScoreForParticipant(Participant.Desc).Score;
        public int Accuracy => Goals != 0 ? Mathf.CeilToInt((float)Goals / Strikes * 100) : 0;

        public readonly IMatchParticipant Participant;
        private readonly ScoreController _scoreController;

        public ParticipantMatchStats(IMatchParticipant participant, ScoreController scoreController)
        {
            Participant = participant;
            _scoreController = scoreController;
        }

        public void AddStrike()
        {
            Strikes++;
            Debug.Log($"{Participant.Desc.Name} Strikes count: {Strikes}, Goals: {Goals}, Accuracy: {Accuracy}%");
        }
    }
}