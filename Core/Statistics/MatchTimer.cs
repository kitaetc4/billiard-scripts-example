using Core;
using Core.Configs;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Scripts.Core
{
    public class MatchTimer : IStartable
    {
        [Inject] private readonly ScoreController _scoreController;

        public float MatchTime
        {
            get
            {
                if (_scoreController.GameEnded) return _endTime - _startTime;
                return Time.realtimeSinceStartup - _startTime;
            }
        }
        
        private float _startTime;
        private float _endTime;

        public MatchTimer(ScoreController scoreController)
        {
            scoreController.OnWin += OnWin;
        }

        private void OnWin(IParticipantDescription winner)
        {
            _endTime = Time.realtimeSinceStartup;
        }

        public void Start()
        {
            _startTime = Time.realtimeSinceStartup;
        }
    }
}