﻿using Core;
using Core.Configs;

namespace Scripts.Core
{
    public class MatchStatistics
    {
        public readonly ParticipantMatchStats StatsLeftParticipant;
        public readonly ParticipantMatchStats StatsRightParticipant;

        private readonly ICurrentParticipantProvider _currentParticipant;
        private readonly StrikeController _strikeController;
        
        public MatchStatistics(ITurnController turnController,
            ICurrentParticipantProvider currentParticipant, 
            StrikeController strikeController, 
            ScoreController scoreController)
        {
            _currentParticipant = currentParticipant;
            _strikeController = strikeController;
            
            StatsLeftParticipant = new ParticipantMatchStats(turnController.LeftParticipant, scoreController);
            StatsRightParticipant = new ParticipantMatchStats(turnController.RightParticipant, scoreController);

            _strikeController.OnBallStricken += OnStrike;
        }

        private void OnStrike(StrikeEventArgs strikeEvent)
        {
            GetCurrentStats().AddStrike();
        }
        
        private ParticipantMatchStats GetCurrentStats()
        {
            return GetStats(_currentParticipant.CurrentParticipant.Desc);
        }
        
        private ParticipantMatchStats GetStats(IParticipantDescription participantDesc)
        {
            if (StatsLeftParticipant.Participant.Desc == participantDesc)
                return StatsLeftParticipant;
            return StatsRightParticipant;
        }
    }
}