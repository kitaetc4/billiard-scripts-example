using UnityEngine;

namespace Core
{
    public class EnergyTransferTester : MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private Vector3 _velocity;
        private float _angle;

        private Vector3 _colPos;
        private Vector3 _dir;
        

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ball"))
            {
                _velocity = _rigidbody.velocity;
            }
        }

        private void OnCollisionExit(Collision collision)
        {
            if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ball"))
            {
                var newVelocity = _rigidbody.velocity;
                var otherObjectVelocity = collision.gameObject.GetComponent<Rigidbody>().velocity;

                var velocityLost = 1 - (newVelocity.magnitude / _velocity.magnitude);
                var velocityPassedToNewObject = otherObjectVelocity.magnitude / _velocity.magnitude;

                _angle = Vector3.Angle(_velocity, otherObjectVelocity);
                
                Debug.Log("Collision!\n" +
                          $"_angle: {_angle}\n" +
                          $"_velocity: {_velocity.magnitude}\n" +
                          $"newVelocity: {newVelocity.magnitude}\n" +
                          $"otherObjectVelocity: {otherObjectVelocity.magnitude}\n" +
                          $"velocityLost: {velocityLost}\n" +
                          $"velocityPassed: {velocityPassedToNewObject}\n" +
                          $"");
                

                _dir = otherObjectVelocity;
                _colPos = transform.position;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawRay(_colPos, _velocity);
            Gizmos.DrawRay(_colPos, _dir);
        }
    }
}