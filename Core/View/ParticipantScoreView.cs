using UnityEngine;

namespace Core.View
{
    public class ParticipantScoreView : MonoBehaviour
    {
        [SerializeField] private AnimatedScoreBallView[] _balls;

        private ParticipantScore _participantScore;
        
        public void AddScore(int score)
        {
            for (int i = 0; i < Mathf.Min(score, _balls.Length); i++)
            {
                _balls[i].TryDrawScoreAnimation();
            }
        }

        private void Start()
        {
            foreach (var ball in _balls)
            {
                ball.Hide();
            }
        }

        public void DrawPlayer(ParticipantScore participantScore)
        {
            _participantScore = participantScore;
            _participantScore.OnScoreChanged += OnScoreChanged;
        }

        private void OnScoreChanged()
        {
            AddScore(_participantScore.Score);
        }

        private void OnDestroy()
        {
            if (_participantScore != null)
            _participantScore.OnScoreChanged -= OnScoreChanged;
        }
    }
}