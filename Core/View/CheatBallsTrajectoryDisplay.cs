﻿using System.Collections.Generic;
using Core.BilliardMath;
using Core.View.Gizmo;
using UnityEngine;
using VContainer;

namespace Core.View
{
    public class CheatBallsTrajectoryDisplay : MonoBehaviour
    {
        [Inject] private BallsProvider _ballsProvider;
        [Inject] private StrikeController _strikeController;
        [Inject] private TrajectoryCalculator _trajectoryCalculator;

        private readonly List<IGizmo> _gizmos = new();

        private void Clear()
        {
            _gizmos.Clear();
        }

        private void OnDrawGizmos()
        {
            if (!enabled) return;
            foreach (var gizmo in _gizmos)
            {
                gizmo.OnDrawGizmos();
            }
        }

        private void Start()
        {
            _strikeController.OnBallStricken += OnBallStricken;
            
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                ball.CollisionEnter += CollisionEnter;
            }
            enabled = false;
        }

        private void OnBallStricken(StrikeEventArgs strike)
        {
            if (!enabled) return;
    
            _gizmos.Clear();

            var impact = _trajectoryCalculator.GetFirstImpact(
                strike.BallStricken.transform.position,
                strike.Direction);
            
            _gizmos.Add(new LineGizmo
            {
                From = strike.BallStricken.transform.position,
                To = impact.BallPosOnCollision,
                Color = Color.red,
            });
            var lastPoint = _trajectoryCalculator.GetLastPoint(impact.BallPosOnCollision, impact.ReflectDir);
            _gizmos.Add(new LineGizmo
            {
                From = impact.BallPosOnCollision,
                To = lastPoint,
                Color = Color.Lerp(Color.red, Color.yellow, 0.5f),
            });

            if (impact.DidHitOtherBall)
            {
                var lastPointOther = _trajectoryCalculator.GetLastPoint(impact.OtherObjectPos, impact.OtherBallRollDirection);
                _gizmos.Add(new LineGizmo
                {
                    From = impact.OtherObjectPos,
                    To = lastPointOther,
                    Color = Color.yellow,
                });   
            }
        }

        private void CollisionEnter(Ball ball, Collision collision)
        {
            if (!enabled) return;
            // Debug.Log("Collision extra distance: " + (Vector3.Distance(ball.transform.position, collision.transform.position) - ball.Radius * 2));
            if (ball == null) return;
            _gizmos.Add(new LineGizmo
            {
                From = ball.transform.position,
                To = ball.Rigidbody.velocity * 30f,
                Color = Color.gray,
            });
            
            _gizmos.Add(new SphereGizmo
            {
                Pos = ball.transform.position,
                Radius = ball.Radius,
                Color = Color.white,
            });
            
            if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ball"))
            {
            }

            if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Bort"))
            {
            }
        }
    }
}