using UnityEngine;

namespace Core.View
{
    public class CueCameraView : MonoBehaviour
    {
        [SerializeField] private Camera _mainCamera;
        private Camera _thisCamera;

        private void Start()
        {
            _thisCamera = GetComponent<Camera>();
        }

        private void LateUpdate()
        {
            _thisCamera.fieldOfView = _mainCamera.fieldOfView;
            _thisCamera.orthographic = _mainCamera.orthographic;
            _thisCamera.orthographicSize = _mainCamera.orthographicSize;
        }
    }
}