using System.Collections;
using UnityEngine;

public class FireLightBlink : MonoBehaviour
{
	Light myLight;

	float maxIntensity = 0.1f;
	float minIntensity = 1f;

	void Start()
	{
		myLight = GetComponent<Light>();
		StartCoroutine(Flicker());
	}

	IEnumerator Flicker()
	{

		float t = 0.0f;
		float duration = Random.Range(0.06f, 0.3f);
		float currIntensity = myLight.intensity;
		float targetIntensity = (currIntensity > 1.0f) ? minIntensity : maxIntensity;
		float variation = Random.Range(0.01f, 0.8f);
		targetIntensity += variation;

		while (t < duration)
		{
			myLight.intensity = Mathf.Lerp(currIntensity, targetIntensity, t / duration);
			t += Time.deltaTime;
			yield return null;
		}

		StartCoroutine(Flicker());
	}
}