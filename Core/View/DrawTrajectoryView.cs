using Core;
using Core.BilliardMath;
using Scripts;
using Scripts.Scriptables;
using States;
using UnityEngine;
using VContainer;

public class DrawTrajectoryView : MonoBehaviour
{
    [SerializeField] private float _line2Lenght;
    [SerializeField] private float _line3Lenght;
    [SerializeField] private LineRenderer _lineRenderer1;
    [SerializeField] private LineRenderer _lineRenderer2;
    [SerializeField] private LineRenderer _lineRenderer3;
    [SerializeField] private GameObject _ghostBall;

    private Vector3 _balloffsetY;

    [Inject] private IMatchModel _currentMatch;
    [Inject] private BallSelectorController _ballSelectorController;
    [Inject] private IGameStateProvider _stateProvider;
    [Inject] private TrajectoryCalculator _trajectoryCalculator;
    [Inject] private GlobalConfig _config;
    private ImpactInfo _impact;

    private void Start()
    {
        gameObject.SetActive(false);
        _balloffsetY = Vector3.up * _config.BallRadius;
        _stateProvider.StateChanged += OnStateChanged;
        _ballSelectorController.OnBallSelectionChanged += OnBallChanged;
    }

    private void OnBallChanged(Ball ball)
    {
        UpdatePos();
    }

    private void OnStateChanged(GameStateEnum newState)
    {
        if (newState == GameStateEnum.StrikeBallState)
        {
            if (_currentMatch.Match.AimLines)
            {
                Draw();
            }       
        }
        else
        {
            Hide();
        }
    }

    private void OnDestroy()
    {
        _stateProvider.StateChanged -= OnStateChanged;
        _ballSelectorController.OnBallSelectionChanged -= OnBallChanged;
    }

    private void UpdatePos()
    {
        if (_ballSelectorController.SelectedBall == null)
        {
            return;
        }

        var ballPos = _ballSelectorController.SelectedBall.transform.position;
        transform.position = new Vector3(ballPos.x, transform.position.y, ballPos.z);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
        _ghostBall.SetActive(false);
    }

    private void Draw()
    {
        UpdatePos();
        gameObject.SetActive(true);
        _ghostBall.SetActive(true);
    }

    private void Update()
    {
        if (!_lineRenderer1.gameObject.activeInHierarchy) return;
        UpdatePos();

        _impact = _trajectoryCalculator.GetFirstImpact(transform.position, transform.forward);
        DrawTrajectoryLines(1, transform.position, _impact.BallPosOnCollision , 0);                    //line1
        DrawTrajectoryLines(2, _impact.BallPosOnCollision, Vector3.zero, _line2Lenght);                //line2
        DrawTrajectoryLines(3, _impact.OtherObjectPos , Vector3.zero, _line3Lenght);     //line3
        _ghostBall.transform.position = _impact.BallPosOnCollision;
    }

    private void DrawTrajectoryLines(int line, Vector3 firstPoint, Vector3 lastPoint, float lenght)
    {
        switch (line)
        {
            case 1:
                DrawLineFromBallToImpact(firstPoint, lastPoint);
                break;
            case 2:
                DrawLineRicochet(firstPoint, lenght);
                break;
            case 3:
                DrawLineOtherBallRollDirection(firstPoint);
                break;
        }
    }

    private void DrawLineOtherBallRollDirection(Vector3 firstPoint)
    {
        if (_impact.DidHitOtherBall)
        {
            _lineRenderer3.gameObject.SetActive(true);
            _lineRenderer3.SetPosition(0, firstPoint - _balloffsetY);
            var lastPoint = _trajectoryCalculator.GetLastPoint(_impact.OtherObjectPos, _impact.OtherBallRollDirection, _line3Lenght);
            _lineRenderer3.SetPosition(1, lastPoint - _balloffsetY);
        }
        else
        {
            _lineRenderer3.gameObject.SetActive(false);
        }
    }

    private void DrawLineRicochet(Vector3 firstPoint, float lenght)
    {
        Vector3 lastPoint;
        _lineRenderer2.SetPosition(0, firstPoint - _balloffsetY);

        lastPoint = _trajectoryCalculator.GetLastPoint(_impact.BallPosOnCollision - _balloffsetY, _impact.ReflectDir,
            lenght * _impact.Angle / 100);

        _lineRenderer2.SetPosition(1, lastPoint);
    }

    private void DrawLineFromBallToImpact(Vector3 firstPoint, Vector3 lastPoint)
    {
        _lineRenderer1.SetPosition(0, firstPoint - _balloffsetY);
        _lineRenderer1.SetPosition(1, lastPoint - _balloffsetY);
    }
}



