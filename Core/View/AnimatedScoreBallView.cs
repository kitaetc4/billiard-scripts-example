using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Core.View
{
    public class AnimatedScoreBallView : MonoBehaviour
    {
        [SerializeField] private RectTransform _ballImage;
        
        private bool _isScored = false;

        [ContextMenu(nameof(Hide))]
        public void Hide()
        {
            _isScored = false;
            _ballImage.gameObject.SetActive(false);
        }
        
        [ContextMenu(nameof(TryDrawScoreAnimation))]
        public void TryDrawScoreAnimation()
        {
            if (_isScored) return;
            _isScored = true;

            DrawScoreAnimation();
        }

        private void DrawScoreAnimation()
        {
            var ballStartPos = new Vector2(0f, -50f);
            var ballStartScale = new Vector2(3f, 3f);
            var ballTargetScale = _ballImage.localScale;

            _ballImage.gameObject.SetActive(true);
            _ballImage.localPosition = ballStartPos;
            _ballImage.localScale = ballStartScale / 2f;

            var img = _ballImage.GetComponent<Image>();
            img.color *= new Color(1f,1f,1f,0f);

            img.DOFade(1f, 0.5f);
            _ballImage.DOScale(ballStartScale, 0.3f).SetEase(Ease.OutElastic).OnComplete(
                () =>
                {
                    _ballImage.DOLocalMoveY(0, 0.3f).SetEase(Ease.InBack).SetDelay(0.3f);
                    _ballImage.DOScale(ballTargetScale, 0.3f).SetEase(Ease.InBack).SetDelay(0.3f);
                }
                );
            
        }
    }
}