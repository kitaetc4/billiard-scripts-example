using System.Collections.Generic;
using Core.Configs;
using Scripts;
using UnityEngine;
using VContainer;

namespace Core.View
{
    public class ParticipantPanelView : MonoBehaviour
    {
        [SerializeField] private ParticipantWidget _leftPlayer;
        [SerializeField] private ParticipantWidget _rightPlayer;
        
        [Inject] private IMatchModel _currentMatch;
        [Inject] private ITurnController _turnController;
        [Inject] private ICurrentParticipantProvider _currentParticipant;
        [Inject] private ScoreController _scoreController;

        private readonly Dictionary<IParticipantDescription, ParticipantWidget> _displays = new();

        private void Start()
        {
            DrawPlayer(_leftPlayer, _currentMatch.Match.Player1);
            DrawPlayer(_rightPlayer, _currentMatch.Match.Player2);

            _displays[_currentMatch.Match.Player1] = _leftPlayer;
            _displays[_currentMatch.Match.Player2] = _rightPlayer;

            _leftPlayer.DeactivateTurnEffect();
            _rightPlayer.DeactivateTurnEffect();
            SwitchParticipant(null, _currentParticipant.CurrentParticipant);
            _currentParticipant.OnSwitchParticipant += SwitchParticipant;
        }

        private void DrawPlayer(ParticipantWidget playerWidget, IParticipantDescription playerDesc)
        {
            if (playerWidget == null) return;
            playerWidget.DrawPlayer(playerDesc, _scoreController.GetScoreForParticipant(playerDesc));
        }


        public void OnDestroy()
        {
            _currentParticipant.OnSwitchParticipant -= SwitchParticipant;
        }
        
        private void SwitchParticipant(IMatchParticipant oldParticipant, IMatchParticipant newParticipant)
        {
            var newParticipantDisplay = _displays[newParticipant.Desc];
            newParticipantDisplay.ActivateTurnEffect();

            if (oldParticipant == null) return;
            var otherDisplay = _displays[oldParticipant.Desc];
            otherDisplay.DeactivateTurnEffect();
        }
    }
}