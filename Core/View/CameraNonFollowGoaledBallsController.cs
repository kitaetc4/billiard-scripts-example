using Cinemachine;
using VContainer;
using VContainer.Unity;

namespace Core.View
{
    public class CameraNonFollowGoaledBallsController : IInitializable
    {
        [Inject] private readonly GoalController _goalController;
        [Inject] private readonly CinemachineTargetGroup _cinemachineTargetGroup;

        public void Initialize()
        {
            _goalController.OnAnyBallGoaled += OnAnyBallGoaled;
        }

        private void OnAnyBallGoaled(Ball ball)
        {
            _cinemachineTargetGroup.RemoveMember(ball.transform);
        }
    }
}