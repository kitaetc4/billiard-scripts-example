using UnityEngine;

public class DrawBallShadow : MonoBehaviour
{
    [SerializeField] GameObject _ballShadow;
    private GameObject instanceShadowPlane;
    private Vector3 ballOffsetY = new Vector3(0f, 0.033f, 0f);
    // Start is called before the first frame update
    void Start()
    {
        instanceShadowPlane = Instantiate(_ballShadow);
    }

    // Update is called once per frame
    void Update()
    {
        instanceShadowPlane.transform.position = transform.position - ballOffsetY;
    }
}
