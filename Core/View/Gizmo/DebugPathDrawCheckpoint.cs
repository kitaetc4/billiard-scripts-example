using UnityEngine;
using System.Collections;

public class DebugPathDrawCheckpoint : MonoBehaviour
{

	public GameObject[] checkpoint;

	void FixedUpdate()
	{
		checkpoint = GameObject.FindGameObjectsWithTag("Checkpoint");
		if (checkpoint.Length > 1)
		{

			for (int i = 0; i < checkpoint.Length - 1; i++)
			{

				Debug.DrawLine(checkpoint[i].transform.position, checkpoint[i + 1].transform.position, Color.green);
			}
		}

	}
}