﻿using UnityEngine;

namespace Core.View.Gizmo
{
    public class LineGizmo : IGizmo
    {
        public Vector3 From;
        public Vector3 To;
        public Color Color = Color.white;

        public void OnDrawGizmos()
        {
            Gizmos.color = Color;
            Gizmos.DrawLine(From, To);
        }
    }
}