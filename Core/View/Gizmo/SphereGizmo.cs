﻿using UnityEngine;

namespace Core.View.Gizmo
{
    public class SphereGizmo : IGizmo
    {
        public Vector3 Pos;
        public float Radius;
        public Color Color = Color.white;
        
        public void OnDrawGizmos()
        {
            Gizmos.color = Color;
            Gizmos.DrawSphere(Pos, Radius);
        }
    }
}