﻿namespace Core.View.Gizmo
{
    public interface IGizmo
    {
        void OnDrawGizmos();
    }
}