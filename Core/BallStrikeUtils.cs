using UnityEngine;

namespace Scripts
{
    public static class BallStrikeUtils
    {
        public static void StrikeBall(Ball ball, Vector3 shotDirection, float power)
        {
            ball.Rigidbody.AddForce(shotDirection * power * ball.ShotPowerMultiply, ForceMode.Impulse);
        }

        public static void SpinBall(Ball ball, Vector3 axisRotation, float power)
        {
            ball.Rigidbody.AddTorque(axisRotation * power, ForceMode.Impulse);
        }
    }
}