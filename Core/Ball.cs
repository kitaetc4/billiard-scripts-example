using System;
using Cysharp.Threading.Tasks;
using Scripts;
using UnityEngine;


public class Ball : MonoBehaviour
{
    public Rigidbody Rigidbody { get; private set; }
    [SerializeField] private float shotPowerMultiply;
    [SerializeField] private Transform shotDirection;

    [SerializeField] private AudioClip _ballBallCollideSound;
    [SerializeField] private AudioClip _ballBortCollideSound;
    [SerializeField] private AudioClip _cueShootSound;
    [SerializeField] private RectTransform _spinShootTarget;
    [SerializeField] public Material ballMaterial;
    [SerializeField] GameObject _ballShadowPrefab;

    private GameObject instanceShadowPlane;

    private AudioSource _audioSource;
    [SerializeField] private AudioSource _audioSourceBGSounds;

    private Vector3 _axisRotation;
    public Vector3 BeforeStrikePosition;
    private Vector3 ballOffsetY = new Vector3(0f, 0.033f, 0f);

    private float _spinPower;

    private bool _isGoaled = false;
    public event Action<Ball> OnGoaled;
    public event Action<Ball> OnGoaledWithFoul;
    public event Action<Ball, Collision> CollisionEnter;

    public bool CollidedWithOtherBall;
    public float Radius => GetComponent<SphereCollider>().radius;
    public float ShotPowerMultiply => shotPowerMultiply;

    void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
        _isGoaled = false;
        instanceShadowPlane = Instantiate(_ballShadowPrefab);
    }

    void Update()
    {
        instanceShadowPlane.transform.position = transform.position - ballOffsetY;
    }

    private void OnCollisionEnter(Collision collision)
    {
        CollisionEnter?.Invoke(this, collision);
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            CollidedWithOtherBall = true;
            PlaySound(_audioSource, _ballBallCollideSound, collision.relativeVelocity.magnitude);
        }

        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Bort"))
        {
            PlaySound(_audioSource, _ballBortCollideSound, collision.relativeVelocity.magnitude / 3);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            CollidedWithOtherBall = true;
        }
    }

    public Vector3 Shot(float power)
    {
        BallStrikeUtils.StrikeBall(this, shotDirection.forward, power);
        GetSpinInfo();
        if (_spinPower != 0f) BallStrikeUtils.SpinBall(this, _axisRotation, _spinPower);
        PlaySound(_audioSource, _cueShootSound, power / 100);
        return shotDirection.forward;
    }

    private void PlaySound(AudioSource audioSource, AudioClip clip, float volume)
    {
        if (audioSource == null || audioSource.isPlaying) return;
        audioSource.clip = clip;
        audioSource.volume = volume;
        audioSource.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_isGoaled && other.gameObject.layer == LayerMask.NameToLayer("Pocket"))
        {
            if (CollidedWithOtherBall)
            {
                _isGoaled = true;
                OnGoaled?.Invoke(this);

                UniTask.Delay(TimeSpan.FromSeconds(6f)).ContinueWith(() => { Destroy(Rigidbody); });
            }
            else
            {
                OnGoaledWithFoul?.Invoke(this);
            }
        }
    }

    private void GetSpinInfo()
    {
        var xAxis = Vector3.Cross(shotDirection.forward, Vector3.up) * (-_spinShootTarget.anchoredPosition.y / 30f);
        var yAxis = Vector3.up * (-_spinShootTarget.anchoredPosition.x / 30f);
        _axisRotation = xAxis + yAxis;
        _spinPower = _axisRotation.magnitude;
    }

    public void FoulBallReturnOnTable()
    {
        Rigidbody.isKinematic = true;
        transform.position = BeforeStrikePosition;
        Rigidbody.isKinematic = false;
    }

    private void OnDestroy()
    {
        Destroy(instanceShadowPlane);
    }
}