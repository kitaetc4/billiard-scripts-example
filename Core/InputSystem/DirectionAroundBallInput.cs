using System;
using Core.Utils;
using Scripts.InputSystem;
using UnityEngine;
using VContainer;
using VContainer.Unity;

public class DirectionAroundBallInput : IDirectionAroundBallInput, ITickable
{
    [Inject] private PinchZoomInput _pinchZoomInput;
    [Inject] private CueUiPowerInput _cueUi;
    private const float RotateByMouseSpeed = 0.05f; // todo вынести в настройки игры
    private const float RotateByKeyboardAcceleration = 0.005f;
    private const float RotateByKeyboardMaxSpeed = 1f;
    private float _currentKeyboardSpeed;
    private bool _enabled;
    private Vector3 _lastFrameMousePos;
    private bool _isDragging;

    public event Action<float> OnAngleUpdate; 


    public void Enable()
    {
        _enabled = true;
    }

    public void Disable()
    {
        _enabled = false;
    }

    public void Tick()
    {
        var mouseDelta = _lastFrameMousePos - Input.mousePosition;
        _lastFrameMousePos = Input.mousePosition;
        
        if (!_enabled) return;
        if (_pinchZoomInput.IsPinching) return;
            
        ProcessMouseInput(mouseDelta);
        ProcessKeyboardInput();
    }

    private void ProcessMouseInput(Vector3 mouseDelta)
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseDelta.x =
                0f; // затычка для телефона: если сняли палец и нажали в другом месте - дельта будет очень большой
            _isDragging = !IsMouseOverUI();
        }

        if (_isDragging && Input.GetMouseButton(0))
        {
            var value = RotateByMouseSpeed * -mouseDelta.x;
            AddAngle(value);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _isDragging = false;
        }
    }

    private void ProcessKeyboardInput()
    {
        _currentKeyboardSpeed =
            Mathf.Clamp(_currentKeyboardSpeed, -RotateByKeyboardMaxSpeed, RotateByKeyboardMaxSpeed);
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            _currentKeyboardSpeed -= RotateByKeyboardAcceleration;
            AddAngle(_currentKeyboardSpeed);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            _currentKeyboardSpeed += RotateByKeyboardAcceleration;
            AddAngle(_currentKeyboardSpeed);
        }
        else
        {
            _currentKeyboardSpeed = 0f;
        }
    }

    private void AddAngle(float angle)
    {
        OnAngleUpdate?.Invoke(angle);
    }

    private bool IsMouseOverUI()
    {
        return UiUtils.IsPointerOverUIElement();
    }
}