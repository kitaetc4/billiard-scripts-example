using System;
using System.Linq;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core
{
    public class BallSelectorInput : ITickable, IBallSelectorInput
    {
        [Inject] private readonly IBallsProvider _ballsProvider;
        
        public event Action<Ball> OnBallClicked;

        private bool _enabled;

        private float _startClickTime;
        private Ball _startedClickOnBall;
        private bool _thresholdDistanceTraveled;
        private Vector3 _startedClickPos;

        private const float _ballClickableRadiusMultiplier = 8f;
        private const float _dragThreshold = 10f;
        private const float _holdDurationThreshold = 1f;

        public void Enable()
        {
            _enabled = true;
        }
        public void Disable()
        {
            _enabled = false;
        }
        
        public void Tick()
        {
            if (!_enabled) return;

            if (Input.GetMouseButtonDown(0))
            {
                _thresholdDistanceTraveled = false;
                _startClickTime = Time.realtimeSinceStartup;
                _startedClickOnBall = GetClickedBall();
                _startedClickPos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0))
            {
                if (Vector3.Distance(_startedClickPos, Input.mousePosition) > _dragThreshold)
                    _thresholdDistanceTraveled = true;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (Time.realtimeSinceStartup - _startClickTime > _holdDurationThreshold) return; // инорируем долгий клик
                if (_thresholdDistanceTraveled) return;
                
                var endClickOnBall = GetClickedBall();

                if (_startedClickOnBall != null && _startedClickOnBall == endClickOnBall)
                {
                    OnBallClicked?.Invoke(endClickOnBall);
                    _startedClickOnBall = null;
                }
            }
        }

        private Ball GetClickedBall()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //Mouse.current.position.ReadValue()
            if (Physics.Raycast(ray, out RaycastHit hitInfo, float.MaxValue))
            {
                if (hitInfo.transform.TryGetComponent<Ball>(out Ball ball))
                {
                    return _ballsProvider.NonGoaled.Contains(ball) ? ball : null;
                }

                // пробуем найти ближайший шарик, если попали жирным пальцем в пустое место на столе
                var nearestBall = _ballsProvider.NonGoaled.MinBy(b => Vector3.Distance(b.transform.position, hitInfo.point));
                if (Vector3.Distance(nearestBall.transform.position, hitInfo.point) < nearestBall.Radius * _ballClickableRadiusMultiplier)
                {
                    return nearestBall;
                }
            }

            return null;
        }
    }
}