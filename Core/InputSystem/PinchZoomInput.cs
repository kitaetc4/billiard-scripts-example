using System;
using VContainer.Unity;
using Core.Utils;
using UnityEngine;
using VContainer;

namespace Scripts.InputSystem
{
    public class PinchZoomInput : ITickable
    {
        private const float ScrollZoomSens = 3f;
        
        [Inject] private readonly CueUiPowerInput _cueUi;

        public bool IsPinching { get; private set; }
        public event Action<float> OnZoomDeltaUpdated;
        public event Action<Vector2> OnZoomCenterMoved;
        
        public Vector3 CurrentZoomCentre { get; private set; }
        private Vector3 _mousePosLastFrame;
        private int _touchCountLastFrame;

        public void Tick()
        {
            IsPinching = Input.touchCount == 2 || _touchCountLastFrame == 2;

            if (Input.GetMouseButtonDown(0))
            {
                _mousePosLastFrame = Input.mousePosition;
            }
            
            if (Input.touchCount == 2)
            {
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);
                
                var touchZeroDp = touchZero.phase == TouchPhase.Began ? Vector2.zero : touchZero.deltaPosition;
                var touchOneDp = touchOne.phase == TouchPhase.Began ? Vector2.zero : touchOne.deltaPosition;
                Vector2 touchZeroPrevPos = touchZero.position - touchZeroDp;
                Vector2 touchOnePrevPos = touchOne.position - touchOneDp;

                float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

                float difference = currentMagnitude - prevMagnitude;

                var centerPosDelta = (touchZeroDp + touchOneDp) / 2;
                if (_touchCountLastFrame == 1)
                {
                    difference = 0f;
                    centerPosDelta = Vector2.zero;
                }
#if UNITY_ANDROID
                difference = -difference;
                centerPosDelta = -centerPosDelta;
#endif
                OnZoomCenterMoved?.Invoke(-centerPosDelta);
                CurrentZoomCentre = Camera.main.ScreenToWorldPoint((touchZero.position + touchOne.position) / 2);

                if (UiUtils.IsPointerOverUIElement() || _cueUi.IsDragged) difference = 0f;
                Zoom(difference * 0.01f);
            }
            else if (Input.GetMouseButton(0))
            {

                if (_mousePosLastFrame == Input.mousePosition || _mousePosLastFrame == Vector3.zero) return;
                if (UiUtils.IsPointerOverUIElement() || _cueUi.IsDragged) return;
                
                if (_touchCountLastFrame == 2) // отпустили палец
                {
                    _mousePosLastFrame = Input.mousePosition; // переписываем позицию чтобы не дёрнулась
                }
                Vector3 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - Camera.main.ScreenToWorldPoint(_mousePosLastFrame);
                var correctedDir = new Vector2(direction.z, -direction.x);
                
                // тут было передвижение камеры однима пальцем при виде сверху.
                // прицеливаться не очень удобно, так что я убрал
                // OnZoomCenterMoved?.Invoke(correctedDir * 600f);
            }

            var scrollZoom = Input.GetAxis("Mouse ScrollWheel");
            if (scrollZoom != 0f)
            {
                CurrentZoomCentre = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Zoom(-scrollZoom * ScrollZoomSens);
            }

            _mousePosLastFrame = Input.mousePosition;
            _touchCountLastFrame = Input.touchCount;
        }

        private void Zoom(float increment)
        {
            OnZoomDeltaUpdated?.Invoke(increment);
        }
    }
}
