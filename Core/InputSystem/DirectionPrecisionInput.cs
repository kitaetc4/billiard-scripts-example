using System;
using Ui;
using VContainer;

namespace Scripts.InputSystem
{
    public class DirectionPrecisionInput : IDirectionPrecisionInput
    {
        [Inject] private InfinityScroll _infinityScroll;
        [Inject] private CueUiPowerInput _cueUi;
        private const float PrecisionScrollerRotateSpeed = 0.01f;
        private bool _enabled;

        public event Action<float> OnAngleUpdate; 


        public void Enable()
        {
            _enabled = true;
            _infinityScroll.OnScrollDelta += ScrollPrecision;
        }

        public void Disable()
        {
            _enabled = false;
            _infinityScroll.OnScrollDelta -= ScrollPrecision;
        }

        private void ScrollPrecision(float value)
        {
            AddAngle(value * PrecisionScrollerRotateSpeed);
        }

        private void AddAngle(float angle)
        {
            OnAngleUpdate?.Invoke(angle);
        }
    }
}