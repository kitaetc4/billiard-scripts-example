using System;
using UnityEngine;

namespace Scripts.InputSystem
{
    public interface IDirectionFromTopInput
    {
        event Action<float> OnAngleUpdate;
        event Action<Vector3> OnTargetUpdate;
        void Enable();
        void Disable();
    }
}