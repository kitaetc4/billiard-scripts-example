using System;

namespace Core
{
    public interface IBallSelectorInput
    {
        event Action<Ball> OnBallClicked;
        void Enable();
        void Disable();
    }
}