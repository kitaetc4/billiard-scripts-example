using System;
using UnityEngine;

namespace InputSystem
{
    public interface IInputManager
    {
        Vector2 GetBallMovement();
    }

    public class InputManager : IDisposable, IInputManager
    {
        private readonly PlayerControls _playerControls = new();

        private Vector2 _dragBallInputLastFrame;

        public void SetDragBallInput(Vector2 input)
        {
            _dragBallInputLastFrame = input;
        }

        public Vector2 GetBallMovement()
        {
            if (_dragBallInputLastFrame != Vector2.zero)
            {
                var result = _dragBallInputLastFrame;
                _dragBallInputLastFrame = default;
                return result;
            }
            return _playerControls.Player.Movement.ReadValue<Vector2>();
        }

        public InputManager()
        {
            _playerControls?.Enable();
        }

        public void Dispose()
        {
            _playerControls?.Dispose();
        }
    }
}
