using System.Text;
using UnityEngine;
using VContainer.Unity;

namespace Scripts.InputSystem
{
    public class TouchDebugger : ITickable
    {
        public void Tick()
        {
            if (Input.touchCount > 0)
            {
                var sb = new StringBuilder();
                sb.Append("count: " + Input.touchCount);
                for (int i = 0; i < Input.touchCount; i++)
                {
                    var touch = Input.GetTouch(i);

                    sb.Append(
                        $"\nid: {touch.fingerId}, phase: {touch.phase}, pos: {touch.position}, delta pos: {touch.deltaPosition}");
                }

            
                Debug.Log(sb.ToString());
            }
        }
    }
}