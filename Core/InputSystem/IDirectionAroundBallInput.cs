using System;

public interface IDirectionAroundBallInput
{
    event Action<float> OnAngleUpdate;
    void Enable();
    void Disable();
}