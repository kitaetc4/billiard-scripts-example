using System;
using Core.Utils;
using Ui;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Scripts.InputSystem
{
    public class DirectionFromTopInput : IDirectionFromTopInput, ITickable
    {
        [Inject] private InfinityScroll _infinityScroll;
        [Inject] private CueUiPowerInput _cueUi;
        private bool _enabled;
        private bool _isDragging;
        private const float RotateByKeyboardAcceleration = 0.005f;
        private const float RotateByKeyboardMaxSpeed = 1f;
        private float _currentKeyboardSpeed;

        public event Action<Vector3> OnTargetUpdate; 
        public event Action<float> OnAngleUpdate; 


        public void Enable()
        {
            _enabled = true;
        }

        public void Disable()
        {
            _enabled = false;
        }

        public void Tick()
        {
            if (!_enabled) return;

            ProcessMouseInput();
            ProcessKeyboardInput();
        }

        private void ProcessMouseInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _isDragging = !IsMouseOverUI();
            }

            if (_isDragging && Input.GetMouseButton(0))
            {
                var target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                OnTargetUpdate?.Invoke(target);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _isDragging = false;
            }
        }

        private void ProcessKeyboardInput()
        {
            _currentKeyboardSpeed =
                Mathf.Clamp(_currentKeyboardSpeed, -RotateByKeyboardMaxSpeed, RotateByKeyboardMaxSpeed);
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                _currentKeyboardSpeed -= RotateByKeyboardAcceleration;
                AddAngle(_currentKeyboardSpeed);
            }
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                _currentKeyboardSpeed += RotateByKeyboardAcceleration;
                AddAngle(_currentKeyboardSpeed);
            }
            else
            {
                _currentKeyboardSpeed = 0f;
            }
        }

        private void AddAngle(float angle)
        {
            OnAngleUpdate?.Invoke(angle);
        }

        private bool IsMouseOverUI()
        {
            return UiUtils.IsPointerOverUIElement();
        }
    }
}