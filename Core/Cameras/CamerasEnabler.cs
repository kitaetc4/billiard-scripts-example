using System;
using System.Threading;
using Cinemachine;
using Core;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class CamerasEnabler : MonoBehaviour
{
    private enum EnabledCamera
    {
        None,
        FollowBall,
        LookAroundTable,
        FromTop
    }
    
    public CinemachineFreeLook VcameraFollowBall => (CinemachineFreeLook)_vCameraFollowBall;
    public CinemachineVirtualCamera VcameraLookAroundTable => (CinemachineVirtualCamera)_vCameraLookAroundTable;
    public CinemachineVirtualCamera VcameraFromTop => (CinemachineVirtualCamera)_vCameraFromTop;
    [SerializeField] private CinemachineVirtualCameraBase _vCameraFollowBall;
    [SerializeField] private CinemachineVirtualCameraBase _vCameraLookAroundTable;
    [SerializeField] private CinemachineVirtualCameraBase _vCameraFromTop;
    [SerializeField] private CinemachineSmoothPath _pathAroundTable;
    [SerializeField] private float _panSpeed = 1f;
    
    [Space]
    [SerializeField] private LayerMask _topCameraLayers = int.MaxValue;
    [SerializeField] private LayerMask _ballCameraLayers = int.MaxValue;
    [SerializeField] private Camera _mainCamera;

    private CameraPathRotationService _posService;
    private CancellationTokenSource _cancellationTokenSource = new();
    private EnabledCamera _lastCamera;
    private CameraPan _cameraPan;

    private void Awake()
    {
        _posService = new CameraPathRotationService(_pathAroundTable);
        _cameraPan = new CameraPan(VcameraLookAroundTable);
    }

    public void FocusOn(Transform transformToFocus)
    {
        _vCameraFollowBall.Follow = transformToFocus;
        _vCameraFollowBall.LookAt = transformToFocus;
    }

    public void ActivateCameraFollowBall()
    {
        if (_lastCamera == EnabledCamera.FollowBall) return;
        _lastCamera = EnabledCamera.FollowBall;
        
        ActivateOnly(_vCameraFollowBall);
        _mainCamera.orthographic = false;

        _cancellationTokenSource = new CancellationTokenSource();
        
        UniTask.Delay(TimeSpan.FromSeconds(1), cancellationToken: _cancellationTokenSource.Token)
            .ContinueWith(() => _mainCamera.cullingMask = _ballCameraLayers);
    }
    
    public void ActivateCameraAroundTable()
    {
        if (_lastCamera == EnabledCamera.LookAroundTable) return;
        _lastCamera = EnabledCamera.LookAroundTable;
        
        _cancellationTokenSource?.Cancel();
        
        SnapCameraAroundTablePositionToCameraOnTable();
        ActivateOnly(_vCameraLookAroundTable);

        _mainCamera.orthographic = false;
        _mainCamera.cullingMask = _ballCameraLayers;
    }
    
    public void ActivateCameraFromTop()
    {
        if (_lastCamera == EnabledCamera.FromTop) return;
        _lastCamera = EnabledCamera.FromTop;
        
        ActivateOnly(_vCameraFromTop);
        _mainCamera.cullingMask = _topCameraLayers;
        SetOrthographics(_mainCamera);
    }

    public void StopFollowingBall()
    {
        FocusOn(null);
    }

    private void SnapCameraAroundTablePositionToCameraOnTable()
    {
        var posOnPath = _posService.FindNearestPosOnPathByRotation(_mainCamera.transform.position, _mainCamera.transform.rotation);
        _vCameraLookAroundTable.transform.position = posOnPath;
    }

    private void ActivateOnly(CinemachineVirtualCameraBase camToActivate)
    {
        foreach (var cam in new [] { _vCameraFollowBall, _vCameraLookAroundTable, _vCameraFromTop})
            cam.gameObject.SetActive(camToActivate == cam);
    }

    private void SetOrthographics(Camera cam)
    {
        cam.orthographic = true;
        cam.orthographicSize = 1.6f;
    }

    private void OnDestroy()
    {
        _cancellationTokenSource?.Cancel();
        _cancellationTokenSource?.Dispose();
    }

    private void Update()
    {
        if (_vCameraLookAroundTable.gameObject.activeInHierarchy)
        {
            _cameraPan.Update(_panSpeed);
        }
        else
        {
            _cameraPan.Reset();
        }
    }
}
