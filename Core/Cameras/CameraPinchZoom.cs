using Core.Utils;
using Scripts.InputSystem;
using TMPro;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core
{
    public class CameraPinchZoom : IInitializable
    {
        private const float _topCamMaxZoom = 1.78f;
        private const float _topCamMinZoom = 0.4f;
        private float _pinchZoomSens = 0.1f;
        private float _pinchMoveSens = 0.003f;
        private float _cameraFollowBallPinchSens = 0.1f;
        private const float _pinchMoveXlimit = 0.85f;
        private const float _pinchMoveZlimit = 1.75f;
        private Vector3 _topCameraBasePos;
        
        [Inject] private readonly CameraController _cameraController;
        [Inject] private readonly CamerasEnabler _camerasEnabler;
        [Inject] private readonly CamerasZoomer _zoomer;
        [Inject] private readonly PinchZoomInput _pinchZoomInput;
        
        TMP_InputField ifzoomsens;
        TMP_InputField ifmovsens;

        public void Initialize()
        {
            _cameraController.OnCameraModeChange += OnCameraModeChange;
            OnCameraModeChange(_cameraController.SelectedCameraMode);
            _topCameraBasePos = _camerasEnabler.VcameraFromTop.transform.position;
        }
        
        private void OnCameraModeChange(CameraModeEnum newMode)
        {
            if (newMode == CameraModeEnum.FromTop)
            {
                _zoomer.ResetTopCameraZoom();

                _pinchZoomInput.OnZoomDeltaUpdated += ZoomTopCam;
                _pinchZoomInput.OnZoomCenterMoved += MoveTopCam;
                
                _pinchZoomInput.OnZoomDeltaUpdated -= ZoomFollowCam;
            }
            else if (newMode == CameraModeEnum.OnBallAndAroundTable)
            {
                _pinchZoomInput.OnZoomDeltaUpdated -= ZoomTopCam;
                _pinchZoomInput.OnZoomCenterMoved -= MoveTopCam;
                
                _pinchZoomInput.OnZoomDeltaUpdated += ZoomFollowCam;
            }
        }

        private void ZoomFollowCam(float delta)
        {
            var current = _camerasEnabler.VcameraFollowBall.m_YAxis.Value;
            var newZoom = current + delta * _cameraFollowBallPinchSens;
            newZoom = Mathf.Clamp01(newZoom);
            _camerasEnabler.VcameraFollowBall.m_YAxis.Value = newZoom;
        }

        private void MoveTopCam(Vector2 delta)
        {
            delta *= _pinchMoveSens;
            var zoom = _camerasEnabler.VcameraFromTop.m_Lens.OrthographicSize;
            var zoomFactor = MathUtils.Remap(zoom, _topCamMinZoom, _topCamMaxZoom, 0.2f, 1f); // чем больше зазумлены, тем медленнее двигаем камеру пинчем
            delta *= zoomFactor;
            _camerasEnabler.VcameraFromTop.transform.position += new Vector3(delta.y, 0f, -delta.x);
            ClampCameraPos();
        }

        private void ZoomTopCam(float delta)
        {
            var current = _camerasEnabler.VcameraFromTop.m_Lens.OrthographicSize;
            var newZoom = current + delta * _pinchZoomSens;
            newZoom = Mathf.Clamp(newZoom, _topCamMinZoom, _topCamMaxZoom);
            _camerasEnabler.VcameraFromTop.m_Lens.OrthographicSize = newZoom;


            if (newZoom == _topCamMinZoom || newZoom == _topCamMaxZoom)
            {
                return;
            }
            
            MoveCameraTowardsZoomCenter(delta);
            ClampCameraPos();
        }

        private void ClampCameraPos()
        {
            var currentZoom = _camerasEnabler.VcameraFromTop.m_Lens.OrthographicSize;
            var currentLimitX = MathUtils.Remap(currentZoom, _topCamMinZoom, _topCamMaxZoom, _pinchMoveXlimit, _topCameraBasePos.x);
            var currentLimitZ = MathUtils.Remap(currentZoom, _topCamMinZoom, _topCamMaxZoom, _pinchMoveZlimit, _topCameraBasePos.z);
            
            var currentPos = _camerasEnabler.VcameraFromTop.transform.position;
            currentPos.x = Mathf.Clamp(currentPos.x, -currentLimitX, currentLimitX);
            currentPos.z = Mathf.Clamp(currentPos.z, -currentLimitZ, currentLimitZ);
            _camerasEnabler.VcameraFromTop.transform.position = currentPos;
        }

        private void MoveCameraTowardsZoomCenter(float delta)
        {
            var centre = _pinchZoomInput.CurrentZoomCentre;
            var currentPos = _camerasEnabler.VcameraFromTop.transform.position;
            var dir = currentPos - centre;
            dir *= delta * _pinchZoomSens;
            _camerasEnabler.VcameraFromTop.transform.position = currentPos + dir;
        }
    }
}