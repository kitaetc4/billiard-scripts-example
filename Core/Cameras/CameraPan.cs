﻿using Cinemachine;
using Core.Utils;
using DG.Tweening;
using UnityEngine;

namespace Core
{
    public class CameraPan
    {
        private const float Ymin = 0.2f;
        private const float Ymax = 0.8f;
        private const float Xmin = 0.1f;
        private const float Xmax = 0.9f;
        private float Xdefault;
        private float Ydefault;
        private float HorizontalDampingDefault;
        private float VerticalDampingDefault;
        
        private CinemachineComposer _composer;
        private Vector3 _lastPos;
        private bool _isResetting = false;
        private bool _isDragging;

        public CameraPan(CinemachineVirtualCamera cam)
        {
            _composer = cam.GetCinemachineComponent<CinemachineComposer>();
            HorizontalDampingDefault = _composer.m_HorizontalDamping;
            VerticalDampingDefault = _composer.m_VerticalDamping;
            Xdefault = _composer.m_ScreenX;
            Ydefault = _composer.m_ScreenY;
        }

        public void Update(float panSpeed)
        {
            if (Input.GetMouseButtonDown(0) && !UiUtils.IsPointerOverUIElement())
            {
                _composer.m_HorizontalDamping = 0.5f; // 1 это почти резкое но немного плавное слежение за целью
                _composer.m_VerticalDamping = 0.5f;
                _isDragging = true;
            }
            else if (Input.GetMouseButton(0) && _isDragging)
            {
                var mouseDelta = _lastPos - Input.mousePosition;
                _composer.m_ScreenX += mouseDelta.x * panSpeed * Time.deltaTime * -1;
                _composer.m_ScreenY += mouseDelta.y * panSpeed * Time.deltaTime;

                _composer.m_ScreenX = Mathf.Clamp(_composer.m_ScreenX, Xmin, Xmax);
                _composer.m_ScreenY = Mathf.Clamp(_composer.m_ScreenY, Ymin, Ymax);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _composer.m_HorizontalDamping = HorizontalDampingDefault;
                _composer.m_VerticalDamping = VerticalDampingDefault;
                _isDragging = false;
            }

            _lastPos = Input.mousePosition;
            _isResetting = false;
        }

        public void Reset()
        {
            if (_isResetting) return;
            _isResetting = true;
            const float transitionDuration = 2f;
            DOTween.To(() => _composer.m_HorizontalDamping, x => _composer.m_HorizontalDamping = x,
                HorizontalDampingDefault, transitionDuration);
            DOTween.To(() => _composer.m_VerticalDamping, x => _composer.m_VerticalDamping = x,
                VerticalDampingDefault, transitionDuration);
            DOTween.To(() => _composer.m_ScreenX, x => _composer.m_ScreenX = x,
                Xdefault, transitionDuration);
            DOTween.To(() => _composer.m_ScreenY, x => _composer.m_ScreenY = x,
                Ydefault, transitionDuration);
        }
    }
}