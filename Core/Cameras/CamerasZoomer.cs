using UnityEngine;

namespace Core
{
    public class CamerasZoomer
    {
        private readonly CamerasEnabler _camerasEnabler;
        private readonly float _baseTopCamZoom;
        private readonly float _followTopCamZoom;
        private readonly Vector3 _baseTopCamPos;

        public CamerasZoomer(CamerasEnabler camerasEnabler)
        {
            _camerasEnabler = camerasEnabler;

            // _baseTopCamZoom = _camerasEnabler.VcameraFromTop.m_Lens.OrthographicSize;
            _baseTopCamZoom = 1.78f; // тут что то ломается, оставлю пока хардкод
            _baseTopCamPos = _camerasEnabler.VcameraFromTop.transform.position;
        }

        public void ResetTopCameraZoom()
        {
            _camerasEnabler.VcameraFromTop.m_Lens.OrthographicSize = _baseTopCamZoom;
            _camerasEnabler.VcameraFromTop.transform.position = _baseTopCamPos;
        }
    }
}