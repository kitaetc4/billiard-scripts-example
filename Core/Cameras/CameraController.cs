using System;
using Cysharp.Threading.Tasks;
using States;
using UnityEngine;

namespace Core
{
    public class CameraController : IDisposable
    {
        private const float StrikePowerToSwitchCamera = 40f;
        private const float CameraStaticTimeAfterStrike = 1f;
        
        private readonly CamerasEnabler _camerasList;
        private readonly BallSelectorController _ballSelector;
        private readonly IGameStateProvider _stateProvider;
        private readonly ICurrentParticipantProvider _currentParticipant;
        private Transform _followedTransform;

        public event Action<CameraModeEnum> OnCameraModeChange; 
        public CameraModeEnum SelectedCameraMode { get; private set; } = CameraModeEnum.OnBallAndAroundTable; // todo сохранять в настройки пользователя

        private GameStateEnum _previousState;

        public CameraController(CamerasEnabler camerasList, BallSelectorController ballSelector, 
            IGameStateProvider stateProvider, ICurrentParticipantProvider currentParticipant)
        {
            _camerasList = camerasList;
            _ballSelector = ballSelector;
            _stateProvider = stateProvider;
            _currentParticipant = currentParticipant;
            _ballSelector.OnBallSelectionChanged += FollowBall;
            _stateProvider.StateChanged += ProcessStateChange;
        }

        private void ProcessStateChange(GameStateEnum newState)
        {
            if (SelectedCameraMode == CameraModeEnum.FromTop)
            {
                _camerasList.ActivateCameraFromTop();
            }

            if (SelectedCameraMode == CameraModeEnum.OnBallAndAroundTable)
            {
                if (newState == GameStateEnum.StrikeBallState)
                {
                    _camerasList.FocusOn(_ballSelector.SelectedBall.transform);
                    _camerasList.ActivateCameraFollowBall();
                }
                else if (newState == GameStateEnum.BallsRollingState)
                {
                    var strikePower = _currentParticipant.CurrentParticipant.LastStrikePower;
                    if (strikePower > StrikePowerToSwitchCamera)
                    {
                        _camerasList.StopFollowingBall();
                        
                        UniTask.Delay(TimeSpan.FromSeconds(CameraStaticTimeAfterStrike)).ContinueWith(
                                _camerasList.ActivateCameraAroundTable);
                    }
                }
                else
                {
                    _camerasList.ActivateCameraAroundTable();
                }
            }
        }

        public void SelectCamera(CameraModeEnum mode)
        {
            SelectedCameraMode = mode;
            OnCameraModeChange?.Invoke(SelectedCameraMode);
            ProcessStateChange(_stateProvider.GetCurrentState());
        }

        public void FollowBall(Ball ball)
        {
            _followedTransform = ball.transform;
            _camerasList.FocusOn(_followedTransform);
        }

        public void Dispose()
        {
            _ballSelector.OnBallSelectionChanged -= FollowBall;
        }
    }

    public enum CameraModeEnum
    {
        OnBallAndAroundTable,
        FromTop
    }
}