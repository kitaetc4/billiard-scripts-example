using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

namespace Core
{
    public class CameraPathRotationService
    {
        private readonly CinemachineSmoothPath _path;
        private Vector3[] _bufferedPositions;
        private const int Iterations = 30;

        public CameraPathRotationService(CinemachineSmoothPath path)
        {
            _path = path;
            _bufferedPositions = CreatePositions();
        }    
        
        public Vector3 FindNearestPosOnPathByRotation(Vector3 centrePos, Quaternion rot)
        {
            var minAngle = float.MaxValue;
            var minAnglePos = Vector3.zero;
            
            for (int i = 0; i < _bufferedPositions.Length; i++)
            {
                var pos = _bufferedPositions[i];
                var currentRotation = Quaternion.LookRotation(centrePos - pos);
                var angle = Quaternion.Angle(currentRotation, rot);
                if (angle < minAngle)
                {
                    minAngle = angle;
                    minAnglePos = pos;
                }
            }

            return minAnglePos;
        }

        private Vector3[] CreatePositions()
        {
            var result = new List<Vector3>();
            var step = _path.PathLength / 50f;

            for (float t = 0; t < _path.PathLength; t += step)
            {
                var pos = _path.EvaluatePosition(t);
                result.Add(pos);
            }
            return result.ToArray();
        }
    }
}