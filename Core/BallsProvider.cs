using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VContainer.Unity;

namespace Core
{
    public interface IBallsProvider
    {
        /// <summary>
        /// Не-забитые шары
        /// </summary>
        IReadOnlyList<Ball> NonGoaled { get; }

        bool IsGoaled(Ball ball);

    }

    public class BallsProvider : IInitializable, IBallsProvider
    {
        /// <summary>
        /// Не-забитые шары
        /// </summary>
        public IReadOnlyList<Ball> NonGoaled => _nonGoaled;
        private List<Ball> _nonGoaled;

        public void Initialize()
        {
            _nonGoaled = GameObject.FindObjectsOfType<Ball>().ToList();

            foreach (var ball in NonGoaled)
            {
                ball.OnGoaled += OnGoaled;
            }
        }

        public bool IsGoaled(Ball ball)
        {
            return !_nonGoaled.Contains(ball);
        }

        public void OnGoaled(Ball ball)
        {
            _nonGoaled.Remove(ball);
            ball.OnGoaled -= OnGoaled;
        }
    }
}