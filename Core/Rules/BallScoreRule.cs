namespace Core.Rules
{
    /// <summary>
    /// Какой шар можно забить в лузы?
    /// </summary>
    public enum BallScoreRule
    {
        /// <summary>
        /// Свой и чужой
        /// </summary>
        OtherAndOwn,
        /// <summary>
        /// Свой (но не с руки) и чужой
        /// </summary>
        OtherAndOwnAfterRicochet,
        /// <summary>
        /// Только чужой
        /// </summary>
        Other,
    }
}