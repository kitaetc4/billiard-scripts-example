namespace Core
{
    public class WinConditionRule
    {
        public readonly int ScoreToWin;

        public WinConditionRule(int scoreToWin)
        {
            this.ScoreToWin = scoreToWin;
        }
    }
}