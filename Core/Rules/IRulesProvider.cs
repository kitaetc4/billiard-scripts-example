namespace Core.Rules
{
    public interface IRulesProvider
    {
        WinConditionRule WinCondition { get; }
        BallScoreRule BallAllowedToScore { get; }
        bool IsAllowedToMoveBallInHome();
        bool CanSelectBall();
    }
}