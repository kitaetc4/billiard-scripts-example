using System;

namespace Core.Rules
{
    /// <summary>
    /// Свободная пирамида
    /// </summary>
    public class RussianFreePyramidRules : IRulesProvider, IDisposable
    {
        private bool _firstStrikeWasMade;
        private readonly StrikeController _strikeController;

        public RussianFreePyramidRules(StrikeController strikeController)
        {
            _strikeController = strikeController;
            _strikeController.OnBallStricken += SetStrikeMade;
        }

        private void SetStrikeMade(StrikeEventArgs obj)
        {
            _firstStrikeWasMade = true;
            _strikeController.OnBallStricken -= SetStrikeMade;
        }

        public WinConditionRule WinCondition => new WinConditionRule(8);
        public BallScoreRule BallAllowedToScore => BallScoreRule.OtherAndOwn;

        public bool IsAllowedToMoveBallInHome()
        {
            return !_firstStrikeWasMade;
        }

        public bool CanSelectBall()
        {
            return _firstStrikeWasMade;
        }

        public void Dispose()
        {
            _strikeController.OnBallStricken -= SetStrikeMade;
        }
    }
}