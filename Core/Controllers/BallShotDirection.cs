using System.Threading.Tasks;
using Cinemachine;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class BallShotDirection : MonoBehaviour
{
    [SerializeField] private CinemachineFreeLook _cinemachineFreeLook;

    private const float FirstStrikeRandomness = 0.1f;
    
    private void Start()
    {
        // Randomize First Strike
        transform.eulerAngles +=  new Vector3(0f, Random.Range(-FirstStrikeRandomness, FirstStrikeRandomness), 0f);
    }

    public async Task AnimateSlowRotateToDirection(Vector3 direction, float maxDegreePerSec = 120f, float minAnimationDuration = 1f)
    {
        var targetRot = Quaternion.LookRotation(direction, Vector3.up).eulerAngles.y;
        var startRot = transform.eulerAngles.y;

        var angleDiff = targetRot - startRot;

        if (Mathf.Abs(Mathf.Abs(angleDiff) - 360f) < angleDiff)
        {
            targetRot -= 360f;
            angleDiff = targetRot - startRot;
        }
        
        var duration = Mathf.Abs(angleDiff / maxDegreePerSec);
        duration = Mathf.Max(minAnimationDuration, duration);

        const Ease ease = Ease.InOutSine;
        // если у тебя тут ошибка то нужно переключить билд таргет на вебгл
        await DOTween.To(() => startRot, (f) => AddAngle(f - transform.eulerAngles.y), targetRot, duration)
            .SetEase(ease);
    }

    public void AddAngle(float value)
    {
        transform.eulerAngles += new Vector3(0f, value, 0f);

        UpdateCameraRotation();
    }
    
    public void SetAimOnTarget(Vector3 target)
    {
        target.y = transform.position.y;
        transform.LookAt(target);
        
        UpdateCameraRotation();
    }

    private void UpdateCameraRotation()
    {
        if (_cinemachineFreeLook != null)
            _cinemachineFreeLook.m_XAxis.Value = transform.eulerAngles.y;
    }
}
