using System.Reflection;
using Core.Rules;
using Core.View;
using NaughtyAttributes;
using States;
using UnityEngine;
using VContainer;

namespace Core
{
    public class CheatsController : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField] private bool _chooseBallOnStart = false;
        [SerializeField] private bool _skipTutor = false;
        [SerializeField] private bool _canSelectBallOnStart = false;
        [Inject] private GameStateEngine _gameStateEngine;
        [Inject] private Tutorial _tutorial;
        [Inject] private IRulesProvider _rules;
        [Inject] private ScoreController _scoreController;
        [Inject] private TurnController _turnController;
        [Inject] private CheatBallsTrajectoryDisplay _trajectory;

        private void Awake()
        {
            if (_chooseBallOnStart)
            {
                _gameStateEngine.FirstState = GameStateEnum.SelectBallState;
            }

            if (_skipTutor)
            {
                _tutorial.Skip();
            }

            if (_canSelectBallOnStart)
            {
                var rp = _rules as RussianFreePyramidRules;
                var field = rp.GetType().GetField("_firstStrikeWasMade", BindingFlags.Instance | BindingFlags.NonPublic);
                field.SetValue(rp, true);
            }
        }

        [Button]
        public void WinPlayer1()
        {
            var player = _turnController.LeftParticipant;

            for (int i = 0; i < _rules.WinCondition.ScoreToWin; i++)
            {
                _scoreController.GetScoreForParticipant(player.Desc).AddBall(null);
            }
        }
        
        [Button]
        public void WinPlayer2()
        {
            var player = _turnController.RightParticipant;

            for (int i = 0; i < _rules.WinCondition.ScoreToWin; i++)
            {
                _scoreController.GetScoreForParticipant(player.Desc).AddBall(null);
            }
        }

        [Button]
        public void ToggleDrawBallTrajectories()
        {
            _trajectory.enabled = !_trajectory.enabled;
        }
#endif
    }

}