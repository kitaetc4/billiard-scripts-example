﻿using UnityEngine;

namespace Core
{
    public class HoleSideCollider : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)] private float _redirectForce = 1f;
        private Transform _holeCenter;

        private void Awake()
        {
            _holeCenter = transform.parent;
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
            {
                var currentBallVelocity = other.rigidbody.velocity;
                
                var directionTowardsHole = (_holeCenter.position - other.transform.position).normalized;
                var velocityTowardsHole = directionTowardsHole * currentBallVelocity.magnitude;

                var newVelocity = velocityTowardsHole * _redirectForce;
                newVelocity.y = other.transform.position.y;
                
                other.rigidbody.velocity = Vector3.Lerp(currentBallVelocity, newVelocity, _redirectForce);
            }
        }
    }
}