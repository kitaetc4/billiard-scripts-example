using System;
using Core;
using Cysharp.Threading.Tasks;
using UnityEngine;
using VContainer;
using DG.Tweening;
using Scripts.Scriptables;
using States;

public class CueController : MonoBehaviour
{
    [SerializeField] private float _rangedivide;
    [SerializeField] private Transform _cueAnim;
    [Inject] private BallShotDirection _target;
    [Inject] private CueUiPowerInput _cueUi;
    [Inject] private VJHandler _spinWidget;
    [Inject] private IGameStateProvider _stateProvider;
    [Inject] private SpinController _spinController;
    [Inject] private GlobalConfig _config;
    
    private const float DelayToHideCueAfterStrike = 1f;

    private void Start()
    {
        transform.position = _target.transform.position;
        _cueAnim.localPosition = Vector3.zero;
        _stateProvider.StateChanged += GameStateUpdated;
        _spinController.OnResetTargetButtonPressed += ResetSpin;
    }

    private void ResetSpin()
    {
        _spinWidget.InputDirection = Vector3.zero;
    }

    private void OnDestroy()
    {
        _stateProvider.StateChanged -= GameStateUpdated;
        _spinController.OnResetTargetButtonPressed -= ResetSpin;
    }

    private async void GameStateUpdated(GameStateEnum newState)
    {
        try
        {
            if (newState == GameStateEnum.StrikeBallState)
            {
                gameObject.SetActive(true);
            }
            else if (gameObject.activeSelf)
            {
                await UniTask.Delay(TimeSpan.FromSeconds(DelayToHideCueAfterStrike));
                gameObject.SetActive(false);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = _target.transform.position;
        transform.DORotate(_target.transform.eulerAngles, 0.5f, RotateMode.Fast);
        if (_spinWidget.InputDirection.x != 0 || _spinWidget.InputDirection.y != 0)
        {
            transform.Translate(_spinWidget.InputDirection * _config.BallRadius);
        }

        if (_cueUi.CurrentStrikePower > 0f)
        {
            _cueAnim.localPosition = -Vector3.forward * _cueUi.CurrentStrikePower * _rangedivide;
        }
        else
        {
            _cueAnim.localPosition = Vector3.zero;
        }
    }
}
