using System;
using Core.Rules;
using Core.Utils;
using JetBrains.Annotations;

namespace Core
{
    public class BallSelectorController : IDisposable
    {
        /// <summary>
        /// null если забит
        /// </summary>
        [CanBeNull] public Ball ColoredBall { get; private set; }

        public bool AnyBallSelected => SelectedBall != null;

        
        public Ball SelectedBall { get; private set; }
        public event Action<Ball> OnBallSelectionChanged; 
        
        private readonly IRulesProvider _rulesProvider;
        private readonly ICurrentParticipantProvider _currentParticipant;

        public BallSelectorController( 
            IRulesProvider rulesProvider, 
            ICurrentParticipantProvider currentParticipant,
            HomeZoneMovableBall movableBall)
        {
            _rulesProvider = rulesProvider;
            _currentParticipant = currentParticipant;
            _currentParticipant.OnSwitchParticipant += OnSwitchParticipant;
            
            ColoredBall = movableBall.GetComponent<Ball>();
            if (!_rulesProvider.CanSelectBall()) // если нельзя выбирать шар на первом ходу то будем бить цветным
            {
                SelectedBall = ColoredBall;
            }
        }

        private void OnSwitchParticipant(IMatchParticipant oldParticipant, IMatchParticipant newParticipant)
        {
            newParticipant.OnBallClicked += TrySelectBall;

            if (oldParticipant != null) oldParticipant.OnBallClicked -= TrySelectBall;
        }

        private void TrySelectBall(Ball ball)
        {
            if (!_rulesProvider.CanSelectBall()) return;
                if (ball == SelectedBall) return;
            if (_currentParticipant.CurrentParticipant.Desc.Brain != Configs.ParticipantBrainType.Bot && 
                UiUtils.IsPointerOverUIElement()) return;

            SelectedBall = ball;
            OnBallSelectionChanged?.Invoke(ball);
        }

        public void Dispose()
        {
            _currentParticipant.CurrentParticipant.OnBallClicked -= TrySelectBall;
            _currentParticipant.OnSwitchParticipant -= OnSwitchParticipant;
        }

        public void Unselect()
        {
            SelectedBall = null;
        }
    }
}