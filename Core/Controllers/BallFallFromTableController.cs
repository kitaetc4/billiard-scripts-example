﻿    using System;
using System.Collections.Generic;
using Scripts.Scriptables;
using VContainer;
using VContainer.Unity;

namespace Core
{
    public class BallFallFromTableController : IInitializable, IFixedTickable
    {
        [Inject] private readonly GlobalConfig _config;
        [Inject] private readonly IBallsProvider _ballsProvider;
        
        private Dictionary<Ball, bool> _ballsFallen = new();
        public event Action<Ball> OnFellFromTable;

        public void Initialize()
        {
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                _ballsFallen[ball] = false;
            }
        }

        public void FixedTick()
        {
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                var fallen = _ballsFallen[ball];

                if (ball.transform.position.y < _config.BallYpositionToConsiderFallen && !fallen)
                {
                    _ballsFallen[ball] = true;
                    OnFellFromTable?.Invoke(ball);
                }

                if (ball.transform.position.y > _config.BallYpositionToConsiderFallen && fallen)
                {
                    _ballsFallen[ball] = false;
                }
            }
        }
    }
}