using System;
using Core;
using Core.Configs;
using UnityEngine;
using UnityEngine.UI;
using VContainer;

public class SpinController : MonoBehaviour
{
    [Inject] private readonly VJHandler _spinWidget;
    [Inject] private readonly ICurrentParticipantProvider _currentParticipant;
    public event Action OnResetTargetButtonPressed;

    [SerializeField] private GameObject _spinWindow;
    [SerializeField] private Button _mainSpinButton;
    [SerializeField] private Button _resetSpinButton;
    [SerializeField] private RectTransform BtnShootTarger;
    [SerializeField] private RectTransform _spinWindowShotTarget;
    [SerializeField] private RectTransform _spinResetContainer;

    private void Start()
    {
        _mainSpinButton.GetComponent<Button>().onClick.AddListener(ToggleWindow);
        _resetSpinButton.GetComponent<Button>().onClick.AddListener(ResetShootTarget);
    }

    private void Update()
    {
        var needToEnable = _spinWidget.InputDirection != Vector3.zero;
        _spinResetContainer.gameObject.SetActive(needToEnable);
    }

    private void ToggleWindow()
    {
        if (_currentParticipant.CurrentParticipant.Desc.Brain != ParticipantBrainType.PlayerControlled) return;
        _spinWindow.SetActive(!_spinWindow.activeInHierarchy);
    }

    public void ResetShootTarget()
    {
        BtnShootTarger.anchoredPosition = Vector2.zero;
        _spinWindowShotTarget.anchoredPosition = Vector2.zero;
        OnResetTargetButtonPressed?.Invoke();
    }
}

