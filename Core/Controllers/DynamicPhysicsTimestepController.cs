﻿using System;
using Core.Utils;
using Scripts.Scriptables;
using Scripts.Ui.WindowsWidgets;
using UnityEngine;
using VContainer.Unity;

namespace Core.Controllers
{
    public class DynamicPhysicsTimestepController : IFixedTickable
    {
        [Serializable]
        public class Settings
        {
            public float MinTimestep = 0.001f;
            public float MaxTimestep = 0.02f;
            public float BoostTimeButtonMultiplier = 2f;
            public int MinTimestepWhenBallsRollingCount = 3;
            public int MaxTimestepWhenBallsRollingCount = 15;
        }
        
        private readonly Settings _settings;
        private readonly BallSettleController _settleController;
        private readonly SpeedUpTimeButton _speedUpTimeButton;

        public DynamicPhysicsTimestepController(GlobalConfig config, BallSettleController settleController,
            SpeedUpTimeButton speedUpTimeButton)
        {
            _settings = config.PhysicsSettings;
            _settleController = settleController;
            _speedUpTimeButton = speedUpTimeButton;
        }

        public void FixedTick()
        {
            var ballsRollingCount = _settleController.BallsRollingCount;
            var timestep = MathUtils.Remap(ballsRollingCount,
                        _settings.MinTimestepWhenBallsRollingCount, _settings.MaxTimestepWhenBallsRollingCount,
                        _settings.MinTimestep, _settings.MaxTimestep);

            var resultFixedStep = Mathf.Clamp(timestep, _settings.MinTimestep, _settings.MaxTimestep);

            if (_speedUpTimeButton.IsPressed)
            {
                Time.timeScale = _settings.BoostTimeButtonMultiplier;
                resultFixedStep *= _settings.BoostTimeButtonMultiplier;
            }
            else
            {
                Time.timeScale = 1f;
            }
            
            Time.fixedDeltaTime = resultFixedStep;
        }
    }
}