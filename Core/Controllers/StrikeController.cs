using System;
using UnityEngine;

namespace Core
{
    public class StrikeController
    {
        private Ball _ball;
        public event Action<StrikeEventArgs> OnBallStricken;

        public void Strike(float power)
        {
            var direction = _ball.Shot(power);
            OnBallStricken?.Invoke(new StrikeEventArgs
            {
                BallStricken = _ball,
                Force = power,
                Direction = direction
            });
        }

        public void SetBall(Ball ball)
        {
            _ball = ball;
        }
    }

    public struct StrikeEventArgs
    {
        public Ball BallStricken;
        public float Force;
        public Vector3 Direction;
    }
}