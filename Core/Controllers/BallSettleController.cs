using System;
using System.Linq;
using UnityEngine;
using VContainer.Unity;

namespace Core
{
    public class BallSettleController : IFixedTickable
    {
        private const float VelocityToStartBraking = 0.1f;
        private const float VelocityConsideredSettled = 0.01f;
        private const float BrakeAmount = 0.999f;

        public bool Settled { get; private set; } = true;
        public int BallsRollingCount { get; private set; }

        public event Action OnSettled;

        private readonly IBallsProvider _ballsProvider;

        public BallSettleController(IBallsProvider ballsProvider)
        {
            _ballsProvider = ballsProvider;
        }

        public void Unsettle()
        {
            Settled = false;
        }

        public void FixedTick()
        {
            if (Settled) return;

            var atLeastSingleBallIsMoving = false;
            var fastestBallVelocity = _ballsProvider.NonGoaled.Max(b => b.Rigidbody.velocity.magnitude);
            if (fastestBallVelocity <= VelocityToStartBraking)
            {
                foreach (var ball in _ballsProvider.NonGoaled)
                {
                    var rb = ball.Rigidbody;
                    rb.velocity *= BrakeAmount;
                    rb.angularVelocity *= BrakeAmount;
                    
                    if (rb.velocity.magnitude <= VelocityConsideredSettled)
                    {
                        rb.velocity = Vector3.zero;
                        rb.angularVelocity = Vector3.zero;
                    }
                    else
                    {
                        atLeastSingleBallIsMoving = true;
                    }
                }
            }
            else
            {
                atLeastSingleBallIsMoving = true;
            }
            
            if (!atLeastSingleBallIsMoving)
            {
                Settled = true;
                BallsRollingCount = 0;
                OnSettled?.Invoke();
            }
            else
            {
                BallsRollingCount = _ballsProvider.NonGoaled.Count(b => !BallIsSettled(b));
            }
        }

        public bool BallIsSettled(Ball ball)
        {
            return (ball.Rigidbody.velocity.magnitude < VelocityConsideredSettled);
        }
    }
}