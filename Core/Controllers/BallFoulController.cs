using System;
using VContainer;
using VContainer.Unity;

namespace Core
{

    public class BallFoulController : IInitializable
    {
        [Inject] private readonly BallFallFromTableController _fallController;
        
        public bool WasFoul { get; private set; } = false;
        public event Action<Ball> OnAnyBallGoaledWithFoul;

        private readonly IBallsProvider _ballsProvider;

        public BallFoulController(IBallsProvider ballsProvider)
        {
            _ballsProvider = ballsProvider;
        }

        public void Initialize()
        {
            _fallController.OnFellFromTable += BallWasGoaledWithFoul;
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                if (ball == null) continue;
                ball.OnGoaledWithFoul += BallWasGoaledWithFoul;
            }
        }

        public void ResetFoul()
        {
            WasFoul = false;
        }

        private void BallWasGoaledWithFoul(Ball ball)
        {
            WasFoul = true;
            ball.FoulBallReturnOnTable();
            OnAnyBallGoaledWithFoul?.Invoke(ball);
        }
    }
}