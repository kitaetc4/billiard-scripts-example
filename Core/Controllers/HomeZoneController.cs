using UnityEngine;

namespace Core
{
    public class HomeZoneController : MonoBehaviour
    {
        public float MaxMinPosX => _maxMinPosX;
        public float MaxPosZ => _maxPosZ;
        public float MinPosZ => _minPosZ;
        
        [SerializeField] private float _maxMinPosX = 0.94f;
        [SerializeField] private float _maxPosZ = -1;
        [SerializeField] private float _minPosZ = -1.77f;

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;

            var avgX = 0f;
            var xExtents = _maxMinPosX * 2f;
            var zExtents = Mathf.Abs(_minPosZ - _maxPosZ);
            var avgZ = (_minPosZ + _maxPosZ) / 2f;
            Gizmos.DrawCube(new Vector3(avgX, 0f, avgZ), new Vector3(xExtents, 0.1f, zExtents));
        }
    }
}