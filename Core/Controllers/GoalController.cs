using System;
using VContainer;
using VContainer.Unity;

namespace Core
{
    public class GoalController : IInitializable
    {
        [Inject] private readonly IBallsProvider _ballsProvider;

        public event Action<Ball> OnAnyBallGoaled;


        public void Initialize()
        {
            foreach (var ball in _ballsProvider.NonGoaled)
            {
                ball.OnGoaled += OnBallGoaled;
            }
        }

        private void OnBallGoaled(Ball ball)
        {
            ball.OnGoaled -= OnBallGoaled;
            OnAnyBallGoaled?.Invoke(ball);
        }
    }
}