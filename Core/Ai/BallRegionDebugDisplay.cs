#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using VContainer.Unity;

namespace Core.Ai
{
    public class BallRegionDebugDisplay : ITickable
    {
        private readonly HoleSelectorService _holeSelectorService;
        private readonly IBallsProvider _ballsProvider;

        public BallRegionDebugDisplay(HoleSelectorService holeSelectorService, IBallsProvider ballsProvider)
        {
            _holeSelectorService = holeSelectorService;
            _ballsProvider = ballsProvider;
        }
        public void Tick()
        {
            var selected = Selection.activeTransform;

            if (!_ballsProvider.NonGoaled.Contains(selected.GetComponent<Ball>())) return;

            foreach (var h in _holeSelectorService.Holes)
            {
                h.DisableGizmos();
            }
            
            var holesIncludingBall = _holeSelectorService.GetIntersectedHoles(selected.GetComponent<Ball>());

            foreach (var h in holesIncludingBall)
            {
                h.EnableGizmos();
            }
        }
    }
}
#endif