using UnityEngine;

namespace Core.Ai
{
    public class HoleAiConfig : MonoBehaviour
    {
        public TriangleArea Triangle { get; private set; }

        [SerializeField] private Transform _posA;
        [SerializeField] private Transform _posB;

        private void Awake()
        {
            Triangle = new TriangleArea(_posA.position, _posB.position, transform.position);
        }



#if UNITY_EDITOR
        private bool _drawGizmos = false;

        private void OnDrawGizmos()
        {
            if (!_drawGizmos) return;
            Gizmos.color = Color.red;
            HandlesUtils.DrawTriangle(Triangle);
        }
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            HandlesUtils.DrawTriangle(Triangle);
        }

        public void DisableGizmos()
        {
            _drawGizmos = false;
        }
        public void EnableGizmos()
        {
            _drawGizmos = true;
        }
#endif
    }
}