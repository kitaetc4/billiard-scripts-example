namespace Core.Ai
{
    public static class BotBrainConfig
    {
        // public const float WaitBeforeSelectBall = 1f;
        // public const float WaitAfterSelectBall = 2f;
        // public const float WaitBeforeAim = 1.5f;
        // public const float WaitAfterAim = 0f;
        // public const float WaitBeforeStrikeBall = 1f;
        // public const float WaitAfterStrikeBall = 2f;
        public const float WaitBeforeSelectBall = 1f;
        public const float WaitAfterSelectBall = 0f;
        public const float WaitBeforeAim = 0f;
        public const float WaitAfterAim = 0f;
        public const float WaitBeforeStrikeBall = 0.5f;
        public const float WaitAfterStrikeBall = 0.5f;
        
        public const float MinStrikePower = 20f;
        public const float MaxStrikePower = 100f;
    }
}