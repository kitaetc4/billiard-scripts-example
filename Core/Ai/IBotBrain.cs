using UnityEngine;

namespace Core.Ai
{
    public interface IBotBrain
    {
        // 1
        public BotAnimatedAction<Ball> SelectBall();
        // 2
        public BotAnimatedAction<Vector3> GetAimDirection();
        // 3
        public BotAnimatedAction<float> Strike();
    }
}