using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Ai
{
    public class RandomBotBrain : IBotBrain
    {
        private readonly IBallsProvider _ballsProvider;
        private readonly BallSelectorController _ballSelectorController;

        public RandomBotBrain(IBallsProvider ballsProvider, BallSelectorController ballSelectorController)
        {
            _ballsProvider = ballsProvider;
            _ballSelectorController = ballSelectorController;
        }

        public BotAnimatedAction<Ball> SelectBall()
        {
            var randomBall = GetRandomBall();
            return new BotAnimatedAction<Ball>(randomBall, BotBrainConfig.WaitBeforeSelectBall, BotBrainConfig.WaitAfterSelectBall);
        }

        public BotAnimatedAction<float> Strike()
        {
            var power = Random.Range(BotBrainConfig.MinStrikePower, BotBrainConfig.MaxStrikePower);
            return new BotAnimatedAction<float>(power, BotBrainConfig.WaitBeforeStrikeBall, BotBrainConfig.WaitAfterStrikeBall);
        }

        public BotAnimatedAction<Vector3> GetAimDirection()
        {
            var randomBall = GetRandomBall();
            var currentBall = _ballSelectorController.SelectedBall;

            var direction = randomBall.transform.position - currentBall.transform.position;
            return new BotAnimatedAction<Vector3>(direction.normalized, BotBrainConfig.WaitBeforeAim, BotBrainConfig.WaitAfterAim);
        }

        private Ball GetRandomBall()
        {
            var randomBall = _ballsProvider.NonGoaled.ToList().GetRandomElement();
            return randomBall;
        }
    }
}