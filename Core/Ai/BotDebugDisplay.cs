using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VContainer;
using Random = UnityEngine.Random;

namespace Core.Ai
{
    public class BotDebugDisplay : MonoBehaviour
    {
#if UNITY_EDITOR
        [Inject] private readonly IBallsProvider _ballsProvider;
        [Inject] private readonly BallSelectorController _ballSelectorController;
        [Inject] private readonly HoleSelectorService _holeSelectorService;
        [Inject] private readonly BallsEstimator _ballsEstimator;
        
        private Vector3 _selectedDirection;

        private void OnDrawGizmos()
        {
            // if (!Application.isPlaying) return;
            // var zonesWithBalls = _holeSelectorService.GetZonesWithBalls();
            //
            // foreach (var zone in zonesWithBalls.Keys)
            // {
            //     var balls = zonesWithBalls[zone];
            //     Handles.Label(zone.transform.position, "BallsCount: " + balls.Count());
            // }
            //
            // var estimates = _ballsEstimator.EstimateAllBallsOnProximityToHole(zonesWithBalls);
            // _ballsEstimator.SortEstimatesFromEasiest(estimates);
            //
            //
            // DrawAllDifficulties(estimates);
            //
            // // Найти самый простой вариант забития. Найти каким шаром можно его забить
            // foreach (var easiestEstimate in estimates)
            // {
            //     // Выбрать шар, которым мы будем бить. Запомнить, куда будем бить
            //     var areaBehindBall = _ballsEstimator.CreateAreaBehindBall(easiestEstimate);
            //     var ballsInArea = _holeSelectorService.GetBallsInArea(areaBehindBall);
            //     // Найти самый простой вариант забития. Найти каким шаром можно его забить
            //     foreach (var easiestEstimate in possibleEstimatedActions)
            //     {
            //         // пока через шар не бьём, только напрямую
            //         if (_ballsEstimator.HasAnyBallInTrajectory(easiestEstimate.Ball, easiestEstimate.StrikeDirection)) continue;
            //         // Выбрать шар, которым мы будем бить. Запомнить, куда будем бить
            //         var areaBehindBall = _ballsEstimator.CreateAreaBehindBall(easiestEstimate);
            //         var ballsInArea = _holeSelectorService.GetBallsInArea(areaBehindBall);
            //         foreach (var ballToStrike in ballsInArea)
            //         {
            //             foreach (var b in ballsInArea)
            //             {
            //                 if (_ballsEstimator.HasOtherBallInTrajectory(ballToStrike, easiestEstimate.Ball, trajectory))
            //                     Handles.color = Color.red;
            //                 else
            //                     Handles.color = Color.green;
            //                 }
            //                 Handles.DrawLine(b.transform.position, easiestEstimate.Ball.transform.position);
            //             }
            //             // Если траекторию пересекает другой шар
            //             var trajectory = _ballsEstimator.GetTrajectory(ballToStrike, easiestEstimate);
            //             if (_ballsEstimator.HasOtherBallInTrajectory(ballToStrike, easiestEstimate.Ball, trajectory))
            //                 continue;
            //             _selectedDirection = trajectory;
            //             Handles.DrawSolidDisc(ballToStrike.transform.position, Vector3.up, 0.02f);
            //             HandlesUtils.DrawTriangle(areaBehindBall);
            //             return;
            //         }
            //     }
            //     
            //     return;
            // }
        }

        private static void DrawAllDifficulties(List<BallStrikeEstimate> estimates)
        {
            var estimatedBalls = new Dictionary<Ball, List<float>>();

            foreach (var e in estimates)
            {
                if (!estimatedBalls.ContainsKey(e.Ball))
                {
                    estimatedBalls[e.Ball] = new List<float>();
                }

                estimatedBalls[e.Ball].Add(e.Difficulty);
            }

            foreach (var ball in estimatedBalls.Keys)
            {
                var allEstimates = string.Join("\n", estimatedBalls[ball]);
                Handles.Label(ball.transform.position, allEstimates);
            }
        }

        public BotAnimatedAction<Ball> SelectBall()
        {
            // Найти все шары, находящиеся в зоне
            var zonesWithBalls = _holeSelectorService.GetZonesWithBalls();
            
            // Оценить простоту забития каждого шара
            var possibleActions = _ballsEstimator.EstimateAllBallsOnProximityToHole(zonesWithBalls);

            // Найти самый простой вариант забития. Найти каким шаром можно его забить
            var easiestAction = possibleActions.MinBy(a => a.Difficulty);

            // Выбрать шар, которым мы будем бить. Запомнить, куда будем бить
            // todo брать не рандом
            var selectedBall = GetRandomBall();

            _selectedDirection = easiestAction.Ball.transform.position - selectedBall.transform.position;
            
            return new BotAnimatedAction<Ball>(selectedBall, BotBrainConfig.WaitBeforeSelectBall, BotBrainConfig.WaitAfterSelectBall);
        }

        public BotAnimatedAction<float> Strike()
        {
            var power = Random.Range(BotBrainConfig.MinStrikePower, BotBrainConfig.MaxStrikePower);
            return new BotAnimatedAction<float>(power, BotBrainConfig.WaitBeforeStrikeBall, BotBrainConfig.WaitAfterStrikeBall);
        }

        public BotAnimatedAction<Vector3> GetAimDirection()
        {
            return new BotAnimatedAction<Vector3>(_selectedDirection, BotBrainConfig.WaitBeforeAim, BotBrainConfig.WaitAfterAim);
        }

        private Ball GetRandomBall()
        {
            var randomBall = _ballsProvider.NonGoaled.ToList().GetRandomElement();
            return randomBall;
        }
#endif
    }
}