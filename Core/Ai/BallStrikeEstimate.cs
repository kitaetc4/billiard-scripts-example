using UnityEngine;

namespace Core.Ai
{
    public struct BallStrikeEstimate
    {
        public readonly Ball Ball { get; }
        public readonly HoleAiConfig TargetHole { get; }
        public readonly float Difficulty { get; }
        public Vector3 DirectionToScore => TargetHole.transform.position - Ball.transform.position;

        public BallStrikeEstimate(Ball ball, HoleAiConfig targetHole, float difficulty)
        {
            Ball = ball;
            TargetHole = targetHole;
            Difficulty = difficulty;
        }
    }
}