using UnityEngine;

namespace Core.Ai
{
    public static class TriangleAreaExtensions
    {
        public static Vector3 GetCenterPos(this TriangleArea tri)
        {
            return (tri.A + tri.B + tri.C) / 3f;
        }

        public static TriangleArea ReverseVertexOrder(this TriangleArea tri)
        {
            return new TriangleArea(tri.C, tri.B, tri.A);
        }

        public static Vector3[] GetVertices(this TriangleArea tri) => new[] { tri.A, tri.B, tri.C };

        /// <summary>
        /// Accurate point in triangle test http://totologic.blogspot.com/2014/01/accurate-point-in-triangle-test.html
        /// Треугльник и точка НЕ считаются находящимися в одной плоскости, высота учитывается
        /// </summary>
        public static bool IsPointInsideTriangleArea(this TriangleArea tri, Vector3 pos)
        {
            return IsPointInsideTriangleAreaInternal(tri, pos) ||
                   IsPointInsideTriangleAreaInternal(tri.ReverseVertexOrder(), pos);
        }

        private static bool IsPointInsideTriangleAreaInternal(TriangleArea tri, Vector3 pos)
        {
            if (!PointInTriangleAreaBoundingBox(tri, pos)) return false;

            if (IsPointOnEdgeOfTriangleArea(tri, pos)) return true;

            var p0 = tri.A;
            var p1 = tri.B;
            var p2 = tri.C;
            var a = 1f / 2f * (-p1.z * p2.x + p0.z * (-p1.x + p2.x) + p0.x * (p1.z - p2.z) + p1.x * p2.z);
            var sign = a < 0f ? -1f : 1f;
            var s = (p0.z * p2.x - p0.x * p2.z + (p2.z - p0.z) * pos.x + (p0.x - p2.x) * pos.z) * sign;
            var t = (p0.x * p1.z - p0.z * p1.x + (p0.z - p1.z) * pos.x + (p1.x - p0.x) * pos.z) * sign;

            return s > 0f && t > 0f && (s + t) < 2f * a * sign;
        }

        public static bool IsPointOnEdgeOfTriangleArea(this TriangleArea tri, Vector3 pos)
        {
            return DistanceToNearestPointOnLine(tri.A, tri.B, pos) <= float.Epsilon ||
                   DistanceToNearestPointOnLine(tri.A, tri.C, pos) <= float.Epsilon ||
                   DistanceToNearestPointOnLine(tri.B, tri.C, pos) <= float.Epsilon;
        }

        public static void GetNearestEdgeToPoint(this TriangleArea tri, Vector3 pos, out Vector3 edgeA, out Vector3 edgeB,
            out float distance)
        {
            float distA = DistanceToNearestPointOnLine(tri.A, tri.B, pos);
            float distB = DistanceToNearestPointOnLine(tri.A, tri.C, pos);
            float distC = DistanceToNearestPointOnLine(tri.B, tri.C, pos);

            if (distA <= distB && distA <= distC)
            {
                distance = distA;
                edgeA = tri.A;
                edgeB = tri.B;
            }
            else if (distB <= distA && distB <= distC)
            {
                distance = distB;
                edgeA = tri.A;
                edgeB = tri.C;
            }
            else //if (distC < distB && distC < distA)
            {
                distance = distC;
                edgeA = tri.B;
                edgeB = tri.C;
            }
        }

        /// <summary>
        /// Accurate point in triangle test http://totologic.blogspot.com/2014/01/accurate-point-in-triangle-test.html
        /// Треугльник и точка всегда считаются находящимися в одной плоскости, высота игнорируется
        /// </summary>
        public static bool IsPointInsideTriangleIgnoreY(this TriangleArea tri, Vector3 pos)
        {
            var posA = tri.A;
            var posB = tri.B;
            var posC = tri.C;
            posA.y = 0f;
            posB.y = 0f;
            posC.y = 0f;
            pos.y = 0f;


            return IsPointInsideTriangleArea(new TriangleArea(posA, posB, posC), pos);
        }

        public static Vector3 ProjectHeight(this TriangleArea tri, Vector3 pos)
        {
            var a = tri.A;
            var ab = tri.B - a;
            var ac = tri.C - a;
            var normal = Vector3.Cross(ab, ac);

            var y = a.y - normal.x * (pos.x - a.x) -
                    normal.z * (pos.z - a.z); // находим через уравнение плоскости, проходящей через точку A
            pos.y = Mathf.Abs(y);
            return pos;
        }

        private static bool PointInTriangleAreaBoundingBox(TriangleArea tri, Vector3 pos)
        {
            var xMin = Mathf.Min(tri.A.x, Mathf.Min(tri.B.x, tri.C.x)) - float.Epsilon;
            var xMax = Mathf.Max(tri.A.x, Mathf.Max(tri.B.x, tri.C.x)) + float.Epsilon;
            var yMin = Mathf.Min(tri.A.y, Mathf.Min(tri.B.y, tri.C.y)) - float.Epsilon;
            var yMax = Mathf.Max(tri.A.y, Mathf.Max(tri.B.y, tri.C.y)) + float.Epsilon;
            var zMin = Mathf.Min(tri.A.z, Mathf.Min(tri.B.z, tri.C.z)) - float.Epsilon;
            var zMax = Mathf.Max(tri.A.z, Mathf.Max(tri.B.z, tri.C.z)) + float.Epsilon;

            return pos.x >= xMin && xMax >= pos.x && pos.y >= yMin && yMax >= pos.y && pos.z >= zMin && zMax >= pos.z;
        }
        
        public static float DistanceToNearestPointOnLine(Vector3 start, Vector3 end, Vector3 point)
        {
            var pointOnLine = FindNearestPointOnLine(start, end, point);
            return Vector3.Distance(point, pointOnLine);
        }

        public static Vector3 FindNearestPointOnLine(Vector3 start, Vector3 end, Vector3 point)
        {
            Vector3 direction = (end - start);
            float magnitudeMax = direction.magnitude;
            direction = direction.normalized;

            Vector3 lhs = point - start;
            float dotP = Vector3.Dot(lhs, direction);
            dotP = Mathf.Clamp(dotP, 0f, magnitudeMax);
            return start + direction * dotP;
        }

    }


}