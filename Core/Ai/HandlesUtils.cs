using UnityEditor;

namespace Core.Ai
{
    public static class HandlesUtils
    {
        public static void DrawTriangle(TriangleArea triangleArea)
        {
#if UNITY_EDITOR
            Handles.DrawLine(triangleArea.A, triangleArea.B);
            Handles.DrawLine(triangleArea.A, triangleArea.C);
            Handles.DrawLine(triangleArea.C, triangleArea.B);
#endif
        }
    }
}