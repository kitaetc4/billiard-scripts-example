using System.Collections.Generic;
using UnityEngine;

namespace Core.Ai
{
    public class BallsEstimator
    {
        public List<BallStrikeEstimate> EstimateAllBallsOnProximityToHole(IReadOnlyDictionary<HoleAiConfig, IEnumerable<Ball>> zonesWithBalls)
        {
            var possibleActions = new List<BallStrikeEstimate>();
            foreach (var zone in zonesWithBalls.Keys)
            {
                var balls = zonesWithBalls[zone];

                foreach (var b in balls)
                {
                    var difficulty = Vector3.Distance(b.transform.position, zone.transform.position);
                    // todo добавить к сложности угол забития (больше угол - сложнее удар)
                    var estimate = new BallStrikeEstimate(b, zone, difficulty);
                    possibleActions.Add(estimate);
                }
            }

            return possibleActions;
        }

        public void SortEstimatesFromEasiest(List<BallStrikeEstimate> estimates)
        {
            estimates.Sort((a, b) => a.Difficulty.CompareTo(b.Difficulty));
        }

        /// <summary>
        /// Создаём треугольную зону, из которой можно без рикошета ударить шар и забить его в лузу
        /// </summary>
        public TriangleArea CreateAreaBehindBall(BallStrikeEstimate estimate)
        {
            var ball = estimate.Ball;
            var hole = estimate.TargetHole;
            var directionFromHole = ball.transform.position - hole.transform.position;
            directionFromHole.Normalize();

            const float absurdlyBigLength = 50f;
            var triangleHeight = directionFromHole * absurdlyBigLength;
            var triangleRightDir = Vector3.Cross(directionFromHole, Vector3.up);
            triangleRightDir.Normalize();

            var a = ball.transform.position;
            var b = a + triangleHeight + triangleRightDir * absurdlyBigLength / 2f; // получается треугольник с углами 60,60,60
            var c = a + triangleHeight - triangleRightDir * absurdlyBigLength / 2f;

            return new TriangleArea(a, b, c);
        }

        public bool HasOtherBallInTrajectory(Ball ballToStrike, Ball ballToHit, Vector3 strikeDirection)
        {
            var layerMask = LayerMask.NameToLayer("Ball");
            Physics.SphereCast(ballToStrike.transform.position, ballToStrike.Radius, strikeDirection, out RaycastHit hit, layerMask);
            return hit.transform != ballToHit.transform;
        }

        public bool HasAnyBallInTrajectory(Ball ballToStrike, Vector3 trajectory)
        {
            var layerMask = LayerMask.NameToLayer("Ball");
            Physics.SphereCast(ballToStrike.transform.position, ballToStrike.Radius, trajectory, out RaycastHit hit, layerMask);
            return hit.transform.TryGetComponent<Ball>(out _);
        }
    }
}