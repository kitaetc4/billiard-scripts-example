using System.Linq;
using Core.BilliardMath;
using Core.Rules;
using Scripts;
using UnityEngine;
using VContainer;
using Random = UnityEngine.Random;

namespace Core.Ai
{
    public class HardBotBrain : IBotBrain
    {
        [Inject] private readonly IBallsProvider _ballsProvider;
        [Inject] private readonly BallSelectorController _ballSelectorController;
        [Inject] private readonly HoleSelectorService _holeSelectorService;
        [Inject] private readonly BallsEstimator _ballsEstimator;
        [Inject] private readonly TrajectoryCalculator _trajectoryCalculator;
        [Inject] private readonly BotStrikePowerCalculator _strikePowerCalculator;
        [Inject] private readonly IMatchModel _currentMatch;
        [Inject] private readonly ScoreController _scoreController;
        [Inject] private readonly ICurrentParticipantProvider _currentParticipant;
        [Inject] private readonly IRulesProvider _rulesProvider;

        private int MyScore => _scoreController.GetScoreForParticipant(_currentMatch.Bot).Score;
        private int EnemyScore => _scoreController.GetScoreForParticipant(_currentMatch.Player).Score;
        
        private Vector3 _selectedDirection;
        private Ball _selectedBall;
        private BallStrikeEstimate? _selectedEstimate;

        public BotAnimatedAction<Ball> SelectBall()
        {
            _selectedBall = CalculateBallSelection();
            
            Debug.Log($"Bot: Selected: {_selectedBall.name}, Will hit: {_selectedEstimate?.Ball}, " +
                      $"Hole: {_selectedEstimate?.TargetHole} Strike Dir: {_selectedDirection}");
            
            return new BotAnimatedAction<Ball>(_selectedBall, BotBrainConfig.WaitBeforeSelectBall, BotBrainConfig.WaitAfterSelectBall);
        }

        private Ball CalculateBallSelection()
        {
            // Найти все шары, находящиеся в зоне
            var zonesWithBalls = _holeSelectorService.GetZonesWithBalls();

            // Оценить простоту забития каждого шара
            var possibleEstimatedActions = _ballsEstimator.EstimateAllBallsOnProximityToHole(zonesWithBalls);
            _ballsEstimator.SortEstimatesFromEasiest(possibleEstimatedActions);

            // todo просчитать с учётом забития нескольких шаров одним ударом

            // Найти самый простой вариант забития. Найти каким шаром можно его забить
            foreach (var easiestEstimate in possibleEstimatedActions)
            {
                // через шар не бьём, только напрямую
                if (_ballsEstimator.HasAnyBallInTrajectory(easiestEstimate.Ball, easiestEstimate.DirectionToScore)) continue;
                // Выбрать шар, которым мы будем бить. Запомнить, куда будем бить
                var areaBehindBall = _ballsEstimator.CreateAreaBehindBall(easiestEstimate);
                var ballsInArea = _holeSelectorService.GetBallsInArea(areaBehindBall);
                foreach (var ballToStrike in ballsInArea)
                {
                    // Если удар невозможен из за слишком крутого угла - скип
                    var ballToStrikePos = ballToStrike.transform.position;
                    var ballToHitPos = easiestEstimate.Ball.transform.position;
                    var positionToScore = easiestEstimate.TargetHole.transform.position;
                    
                    if (!_trajectoryCalculator.IsStrikePossibleByAngle(ballToStrikePos, ballToHitPos, positionToScore))
                        continue;
                    
                    // Если траекторию пересекает другой шар - скип
                    var targetPos = _trajectoryCalculator.CalculateCollisionPosToScore(ballToStrikePos, ballToHitPos, positionToScore);
                    var strikeDirection = targetPos - ballToStrikePos;
                    if (_ballsEstimator.HasOtherBallInTrajectory(ballToStrike, easiestEstimate.Ball, strikeDirection))
                        continue;
                    _selectedDirection = AddRandomAim(strikeDirection);
                    _selectedEstimate = easiestEstimate;
                    return ballToStrike;
                }
            }
            // todo просчитать с учётом рикошета от стены
            
            Debug.Log("Bot: Choosing random ball");
            var randomBallToStrike = GetRandomBall();
            var randomBallToHit = GetRandomBall();
            _selectedDirection = randomBallToHit.transform.position - randomBallToStrike.transform.position;
            return randomBallToStrike;
        }

        private Vector3 AddRandomAim(Vector3 aim)
        {
            var difficultySettings = _currentMatch.Mission.Difficulty;
            var randomness = difficultySettings.BotAddRandomStrikeAngle;
            randomness *= difficultySettings.InaccuracyMultiplierForEveryConsecutiveGoal 
                * _scoreController.ConsecutiveGoalsOnThisTurn + 1;
            if (MyScore == _rulesProvider.WinCondition.ScoreToWin - 1)
            {
                randomness *= difficultySettings.InaccuracyMultiplierOnLastGoal;
            }            
            if (EnemyScore == _rulesProvider.WinCondition.ScoreToWin - 1)
            {
                randomness *= difficultySettings.InaccuracyMultiplierWhenPlayerIsOnLastBall;
            }
            return aim + new Vector3(Random.Range(-randomness, randomness), 0f, Random.Range(-randomness, randomness));
        }

        public BotAnimatedAction<float> Strike()
        {
            float power = 0f;
            if (_selectedEstimate == null)
            {
                power = Random.Range(BotBrainConfig.MinStrikePower, BotBrainConfig.MaxStrikePower);
                Debug.Log("Bot random strike power: " + power);
            }
            else
            {
                power = _strikePowerCalculator.CalculateStrikePower(_selectedBall,
                    _selectedEstimate.Value.Ball,
                    _selectedEstimate.Value.TargetHole.transform.position);;
                power = Mathf.Clamp(power, BotBrainConfig.MinStrikePower, BotBrainConfig.MaxStrikePower);
                Debug.Log("Bot calculated strike power: " + power);
            }
            return new BotAnimatedAction<float>(power, BotBrainConfig.WaitBeforeStrikeBall, BotBrainConfig.WaitAfterStrikeBall);
        }

        public BotAnimatedAction<Vector3> GetAimDirection()
        {
            return new BotAnimatedAction<Vector3>(_selectedDirection, BotBrainConfig.WaitBeforeAim, BotBrainConfig.WaitAfterAim);
        }

        private Ball GetRandomBall()
        {
            var randomBall = _ballsProvider.NonGoaled.ToList().GetRandomElement();
            return randomBall;
        }
    }
}