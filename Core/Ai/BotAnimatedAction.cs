namespace Core.Ai
{
    public class BotAnimatedAction<T>
    {
        public readonly float DelayBefore;
        public readonly T Result;
        public readonly float DelayAfter;

        public BotAnimatedAction(T result, float delayBefore = 0f, float delayAfter = 0f)
        {
            DelayBefore = delayBefore;
            Result = result;
            DelayAfter = delayAfter;
        }
    }
}