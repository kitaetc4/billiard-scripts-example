using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Ai
{
    public class HoleSelectorService
    {
        public HoleAiConfig[] Holes => _holes;
        private readonly TriangleArea[] _areas;
        private readonly HoleAiConfig[] _holes;
        private readonly IBallsProvider _ballsProvider;

        public HoleSelectorService(HoleAiConfig[] holes, IBallsProvider ballsProvider)
        {
            _holes = holes;
            _ballsProvider = ballsProvider;
            _areas = holes.Select(h => h.Triangle).ToArray();
        }

        public bool BallIsInAnyArea(Ball ball)
        {
            return _areas.Any(a => a.IsPointInsideTriangleIgnoreY(ball.transform.position));
        }

        public HoleAiConfig SelectBestHoleToScoreBall(Ball ball)
        {
            // todo тут выбрать нормально по парам шаров
            return GetNearestHole(ball.transform.position);
        }

        public HoleAiConfig GetNearestHole(Vector3 pos)
        {
            return _holes.MinBy(h => Vector3.Distance(h.transform.position, pos));
        }

        public List<HoleAiConfig> GetIntersectedHoles(Ball ball)
        {
            return _holes
                .Where(h => h.Triangle.IsPointInsideTriangleIgnoreY(ball.transform.position))
                .ToList();
        }

        public IReadOnlyDictionary<HoleAiConfig, IEnumerable<Ball>> GetZonesWithBalls()
        {
            var result = new Dictionary<HoleAiConfig, IEnumerable<Ball>>();
            foreach (var hole in _holes)
            {
                var ballsInArea = _ballsProvider.NonGoaled.Where(
                    b => hole.Triangle.IsPointInsideTriangleIgnoreY(b.transform.position));
                result.Add(hole, ballsInArea);
            }

            return result;
        }

        public IEnumerable<Ball> GetBallsInArea(TriangleArea area)
        {
            return _ballsProvider.NonGoaled.Where(b => area.IsPointInsideTriangleIgnoreY(b.transform.position));
        }
    }
}