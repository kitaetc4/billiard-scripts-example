using Core.Configs;
using UnityEngine;

namespace Core.Ai
{
    public class BotStrikePowerCalculator
    {
        private readonly LookupTableManager _lookupTableManager;

        public BotStrikePowerCalculator(LookupTableManager lookupTableManager)
        {
            _lookupTableManager = lookupTableManager;
        }

        public float CalculateStrikePower(Ball ballToStrike, Ball ballToHit, Vector3 holePosition)
        {
            var a = ballToHit.transform.position;
            var b = ballToStrike.transform.position;
            var c = holePosition;
            var distanceAb = Vector3.Distance(a, b);
            var distanceBc = Vector3.Distance(b, c);
            var angle = Vector3.Angle(b - a, b - c);
            var powerLossTable = _lookupTableManager.CollisionEnergy;
            var powerLossFromAngle = powerLossTable.CalculateY(angle);
            var totalDistModified = distanceAb + distanceBc / powerLossFromAngle;
            var strikeTable = _lookupTableManager.StrikePower;
            var strikePowerFromDistance = strikeTable.CalculateX(totalDistModified);
            return strikePowerFromDistance;
        }
    }
}