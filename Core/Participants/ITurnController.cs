namespace Core
{
    public interface ITurnController
    {
        IMatchParticipant LeftParticipant { get; }
        IMatchParticipant RightParticipant { get; }
    }
}