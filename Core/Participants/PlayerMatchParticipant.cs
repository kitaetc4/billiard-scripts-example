using System;
using Core.Configs;
using InputSystem;
using Scripts.InputSystem;
using States;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core
{
    public class PlayerMatchParticipant : ITickable, IMatchParticipant
    {
        [Inject] private readonly IInputManager _inputManager;
        [Inject] private readonly CueUiPowerInput _cueUi;
        [Inject] private readonly BallSelectorInput _ballSelectorInput;
        [Inject] private readonly IDirectionPrecisionInput _directionPrecisionInput;
        [Inject] private readonly IDirectionAroundBallInput _directionAroundBallInput;
        [Inject] private readonly IDirectionFromTopInput _directionFromTopInput;
        [Inject] private readonly BallShotDirection _direction;
        [Inject] private readonly CameraController _cameraController;
        [Inject] private readonly IGameStateProvider _gameStateProvider;
        private bool _enabled;
        public event Action<float> OnStrike;
        public event Action<Vector2> OnBallMoveInHomeZone;
        public event Action<Ball> OnBallClicked;

        public IParticipantDescription Desc { get; }
        public float LastStrikePower { get; private set; }

        public bool IsBot => false;

        public PlayerMatchParticipant(IParticipantDescription desc)
        {
            Desc = desc;
        }

        public void Tick()
        {
            if (!_enabled) return;
            var playerMovement = _inputManager.GetBallMovement();
            if (playerMovement == Vector2.zero) return;
            OnBallMoveInHomeZone?.Invoke(playerMovement);
        }
        
        public void Enable()
        {
            _enabled = true;
            _ballSelectorInput.Enable();
            _ballSelectorInput.OnBallClicked += OnBallClick;
            _directionPrecisionInput.Enable();
            _directionPrecisionInput.OnAngleUpdate += OnAngleUpdate;
            _directionAroundBallInput.OnAngleUpdate += OnAngleUpdate;
            _directionFromTopInput.OnAngleUpdate += OnAngleUpdate;
            _directionFromTopInput.OnTargetUpdate += OnTargetUpdate;
            _cameraController.OnCameraModeChange += OnCameraModeChange;
            OnCameraModeChange(_cameraController.SelectedCameraMode);
            _cueUi.Enable();
            _cueUi.OnStrike += Strike;
        }

        private void OnCameraModeChange(CameraModeEnum newMode)
        {
            if (newMode == CameraModeEnum.FromTop)
            {
                _directionFromTopInput.Enable();
                _directionAroundBallInput.Disable();
            }
            else if (newMode == CameraModeEnum.OnBallAndAroundTable)
            {
                _directionFromTopInput.Disable();
                _directionAroundBallInput.Enable();
            }
        }

        public void Disable()
        {
            _enabled = false;
            _ballSelectorInput.Disable();
            _ballSelectorInput.OnBallClicked -= OnBallClick;
            _directionPrecisionInput.Disable();
            _directionPrecisionInput.OnAngleUpdate -= OnAngleUpdate;
            _directionAroundBallInput.Disable();
            _directionAroundBallInput.OnAngleUpdate -= OnAngleUpdate;
            _directionFromTopInput.Disable();
            _directionFromTopInput.OnTargetUpdate -= OnTargetUpdate;
            _directionFromTopInput.OnAngleUpdate -= OnAngleUpdate;
            _cameraController.OnCameraModeChange -= OnCameraModeChange;
            _cueUi.Disable();
            _cueUi.OnStrike -= Strike;
        }

        private void OnTargetUpdate(Vector3 target)
        {
            _direction.SetAimOnTarget(target);
        }

        private void OnAngleUpdate(float angleDelta)
        {
            _direction.AddAngle(angleDelta);
        }

        private void OnBallClick(Ball ball)
        {
            if (_gameStateProvider.GetCurrentState() == GameStateEnum.SelectBallState)
            {
                if (_cameraController.SelectedCameraMode == CameraModeEnum.OnBallAndAroundTable)
                {
                    AimTowardsCameraLookDirection(ball);
                }
            }
            
            OnBallClicked?.Invoke(ball);
        }

        private void AimTowardsCameraLookDirection(Ball ball)
        {
            var direction = ball.transform.position - Camera.main.transform.position;
            _direction.SetAimOnTarget(ball.transform.position + direction);
        }

        private void Strike(float power)
        {
            LastStrikePower = power;
            OnStrike?.Invoke(power);
        }
    }
}