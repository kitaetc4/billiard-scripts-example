using System;
using System.Threading.Tasks;
using Core.Ai;
using Core.Configs;
using Cysharp.Threading.Tasks;
using InputSystem;
using States;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core
{
    public class BotMatchParticipant : ITickable, IMatchParticipant
    {
        [Inject] private readonly IInputManager _inputManager;
        [Inject] private readonly CueUiPowerInput _cueUi;
        [Inject] private readonly IGameStateProvider _gameStateProvider;
        [Inject] private readonly ICurrentParticipantProvider _currentParticipant;
        [Inject] private readonly BallSelectorController _selectorController;
        [Inject] private readonly IBotBrain _brain;
        [Inject] private readonly BallShotDirection _ballShotDirection;
        [Inject] private readonly SpinController _spinController;

        private bool _enabled;
        public event Action<float> OnStrike;
        public event Action<Vector2> OnBallMoveInHomeZone;
        public event Action<Ball> OnBallClicked;

        public IParticipantDescription Desc { get; }
        public float LastStrikePower { get; private set; }

        public bool IsBot => true;

        public void Enable()
        {
            _enabled = true;
        }

        public void Disable()
        {
            _enabled = false;
        }

        public BotMatchParticipant(IParticipantDescription desc)
        {
            Desc = desc;
        }

        [Inject]
        public void Initialize()
        {
            _gameStateProvider.StateChanged += OnGameStateChanged;
        }

        public async void OnGameStateChanged(GameStateEnum newState)
        {
            try
            {
                switch (newState)
                {
                    case GameStateEnum.BallsRollingState:
                        Idle();
                        break;

                    case GameStateEnum.SelectBallState:
                        if (_enabled)
                        {
                            ResetSpin();
                            SelectBall();
                        }

                        break;

                    case GameStateEnum.FoulPickBallState:
                        if (_enabled)
                        {
                            ResetSpin();
                            SelectBall();
                        }

                        break;

                    case GameStateEnum.StrikeBallState:
                        if (_enabled)
                        {
                            await Aim();
                            await StrikeBall();
                        }

                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void ResetSpin()
        {
            _spinController.ResetShootTarget();
        }

        private async Task Aim()
        {
            Debug.Log("Bot: AimAndStrikeBall");

            var aim = _brain.GetAimDirection();

            await Delay(aim.DelayBefore);

            await _ballShotDirection.AnimateSlowRotateToDirection(aim.Result);

            // var direction = aim.Result;
            // var target = _ballShotDirection.transform.position + direction;
            // _ballShotDirection.SetAimOnTarget(target);

            await Delay(aim.DelayAfter);
        }

        private async Task StrikeBall()
        {
            const float strikeAnimDuration = 1f;
            var strike = _brain.Strike();

            await Delay(strike.DelayBefore);

            await _cueUi.AnimateStrike(strike.Result, strikeAnimDuration);

            ResetSpin();
            LastStrikePower = strike.Result;
            OnStrike?.Invoke(strike.Result);

            await Delay(strike.DelayBefore);
        }

        private async Task SelectBall()
        {
            Debug.Log("Bot: SelectBall");

            var selectBall = _brain.SelectBall();

            await Delay(selectBall.DelayBefore);

            OnBallClicked?.Invoke(selectBall.Result);

            await Delay(selectBall.DelayAfter);
        }

        private void Idle()
        {
            Debug.Log("Bot: Idle");
        }

        private async Task Delay(float durationSec)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(durationSec));
        }

        // todo науить бота шевелить шар в хоумзоне
        public void Tick()
        {
            //     var playerMovement = _inputManager.GetBallMovement();
            //     if (playerMovement == Vector2.zero) return;
            //     OnBallMoveInHomeZone?.Invoke(playerMovement);
        }
    }
}