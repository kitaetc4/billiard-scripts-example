using System;
using System.Collections.Generic;
using Core.Configs;
using Core.Rules;
using Scripts;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Core
{
    public class ScoreController : IDisposable, IInitializable
    {
        public event Action<IParticipantDescription> OnWin;

        [Inject] private readonly GoalController _goalController;
        [Inject] private readonly ICurrentParticipantProvider _currentParticipantProvider;
        [Inject] private readonly IMatchModel _currentMatch;
        [Inject] private readonly IRulesProvider _rules;

        private ParticipantScore _scorePlayer1;
        private ParticipantScore _scorePlayer2;
        public bool GameEnded { get; private set; } = false;
        public int ConsecutiveGoalsOnThisTurn { get; private set; }

        public void Initialize()
        {
            _scorePlayer1 = new ParticipantScore();
            _scorePlayer2 = new ParticipantScore();

            _goalController.OnAnyBallGoaled += AnyBallGoaled;

            _scorePlayer1.OnScoreChanged += CheckWinCondition;
            _scorePlayer2.OnScoreChanged += CheckWinCondition;
            _currentParticipantProvider.OnSwitchParticipant += ClearConsecutiveCounter;
        }

        public void Dispose()
        {
            _goalController.OnAnyBallGoaled -= AnyBallGoaled;

            _scorePlayer1.OnScoreChanged -= CheckWinCondition;
            _scorePlayer2.OnScoreChanged -= CheckWinCondition;
        }

        public ParticipantScore GetScoreForParticipant(IParticipantDescription desc)
        {
            if (_currentMatch.Match.Player1 == desc) return _scorePlayer1;
            else return _scorePlayer2;
        }

        private void AnyBallGoaled(Ball ball)
        {
            if (GameEnded) return;
            var participant = _currentParticipantProvider.CurrentParticipant;
            GetScoreForParticipant(participant.Desc).AddBall(ball);
            ConsecutiveGoalsOnThisTurn++;
            Debug.Log("ConsecutiveGoalsOnThisTurn: " + ConsecutiveGoalsOnThisTurn);
        }

        private void ClearConsecutiveCounter(IMatchParticipant arg1, IMatchParticipant arg2)
        {
            ConsecutiveGoalsOnThisTurn = 0;
        }


        private void CheckWinCondition()
        {
            if (!CheckWin(_currentMatch.Match.Player1))
            {
                CheckWin(_currentMatch.Match.Player2);
            }
        }

        private bool CheckWin(IParticipantDescription desc)
        {
            var score = GetScoreForParticipant(desc);
            if (score.Score < _rules.WinCondition.ScoreToWin) return false;
            
            GameEnded = true;
            OnWin?.Invoke(desc);
            Debug.Log("Player won: " + desc.Name);
            return true;
        }
    }
}