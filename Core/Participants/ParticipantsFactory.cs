using System.Linq;
using Core.Configs;
using Scripts;
using VContainer;

namespace Core
{
    public class ParticipantsFactory
    {
        [Inject] private readonly IMatchModel _matchConfig;
        [Inject] private readonly ParticipantDescriptionSdkPlayerData _sdkPlayer;
        
        public IMatchParticipant[] CreateParticipants(IObjectResolver resolver)
        {
            var leftPlayer = CreateParticipant(_sdkPlayer);
            var rightPlayer = CreateParticipant(_matchConfig.Match.Player2);
            resolver.Inject(leftPlayer);
            resolver.Inject(rightPlayer);
            
            var participants = new IMatchParticipant[] { leftPlayer, rightPlayer };
            if (!_matchConfig.Mission.PlayerStrikesFirst)
            {
                participants = participants.Reverse().ToArray();
            }
            return participants;
        }

        private IMatchParticipant CreateParticipant(IParticipantDescription desc)
        {
            if (desc.Brain == ParticipantBrainType.PlayerControlled)
            {
                return new PlayerMatchParticipant(desc);
            }
            return new BotMatchParticipant(desc);
        }
    }
}