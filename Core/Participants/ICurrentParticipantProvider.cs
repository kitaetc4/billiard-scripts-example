using System;

namespace Core
{
    public interface ICurrentParticipantProvider
    {
        event Action<IMatchParticipant, IMatchParticipant> OnSwitchParticipant;
        IMatchParticipant CurrentParticipant { get; }
        bool IsPlayerTurn => !CurrentParticipant.IsBot;
        bool IsBotTurn => CurrentParticipant.IsBot;
        bool IsFirstTurn { get; }
    }
}