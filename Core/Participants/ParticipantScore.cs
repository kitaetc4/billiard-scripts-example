using System;
using System.Collections.Generic;
using Core.Configs;

namespace Core
{
    public class ParticipantScore
    {
        public event Action OnScoreChanged;
        public int Score => BallsScored.Count;
        public IReadOnlyList<Ball> BallsScored => _ballsScored;
        public List<Ball> _ballsScored = new();
        
        public void AddBall(Ball ball)
        {
            _ballsScored.Add(ball);
            OnScoreChanged?.Invoke();
        }
    }
}