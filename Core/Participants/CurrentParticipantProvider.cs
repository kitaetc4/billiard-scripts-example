using System;

namespace Core
{
    public class CurrentParticipantProvider : ICurrentParticipantProvider
    {
        public event Action<IMatchParticipant, IMatchParticipant> OnSwitchParticipant;
        public IMatchParticipant CurrentParticipant { get; private set; }
        public bool IsFirstTurn { get; private set; }

        public void SwitchParticipant(IMatchParticipant newParticipant)
        {
            IsFirstTurn = false;
            var old = CurrentParticipant;
            CurrentParticipant = newParticipant;
            OnSwitchParticipant?.Invoke(old, newParticipant);
        }

        public void SetFirstParticipant(IMatchParticipant matchParticipant)
        {
            IsFirstTurn = true;
            CurrentParticipant = matchParticipant;
            OnSwitchParticipant?.Invoke(null, CurrentParticipant);
        }
    }
}