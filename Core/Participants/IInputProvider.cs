using System;
using UnityEngine;

namespace Core
{
    /// <summary>
    /// Ввод от участника матча - либо ИИ либо игрока
    /// </summary>
    public interface IInputProvider
    {
        public event Action<float> OnStrike;
        public event Action<Vector2> OnBallMoveInHomeZone;
        public event Action<Ball> OnBallClicked;

    }
}