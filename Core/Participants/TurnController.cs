using System;
using System.Linq;
using UnityEngine;

namespace Core
{
    public class TurnController : ITurnController
    {
        public IMatchParticipant LeftParticipant => _playersPair[0];
        public IMatchParticipant RightParticipant => _playersPair[1];

        private readonly IMatchParticipant[] _playersPair;
        private readonly CurrentParticipantProvider _currentParticipantProvider;

        public TurnController(Func<IMatchParticipant[]> participantsFactory,
            CurrentParticipantProvider currentParticipantProvider)
        {
            _playersPair = participantsFactory();
            
            _currentParticipantProvider = currentParticipantProvider;
            StartPvpGame();

        }

        public void StartPvpGame()
        {
            _currentParticipantProvider.SetFirstParticipant(_playersPair[0]);
            _currentParticipantProvider.CurrentParticipant.Enable();
        }

        public void SwitchToNextPlayer()
        {
            var i = _playersPair.ToList().IndexOf(_currentParticipantProvider.CurrentParticipant);
            var newParticipant = _playersPair[++i % 2];
            Debug.Log($"TurnController.SwitchToNextPlayer newParticipant: " + newParticipant.Desc.Name);
            var old = _currentParticipantProvider.CurrentParticipant;
            
            _currentParticipantProvider.SwitchParticipant(newParticipant);
            
            old.Disable();
            newParticipant.Enable();
        }
    }
}