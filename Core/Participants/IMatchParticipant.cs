using Core.Configs;
using UnityEngine;

namespace Core
{
    public interface IMatchParticipant : IInputProvider
    {
        IParticipantDescription Desc { get; }
        bool IsBot { get; }
        void Enable();
        void Disable();
        float LastStrikePower { get; }
    }
}