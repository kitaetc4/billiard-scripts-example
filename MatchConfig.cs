using System;
using Core.Configs;

namespace Core
{
    [Serializable]
    public class MatchConfig
    {
        public readonly IParticipantDescription Player1;
        public readonly IParticipantDescription Player2;
        public bool AimLines;

        public MatchConfig(IParticipantDescription player1, 
            IParticipantDescription player2, bool aimLines)
        {
            Player1 = player1;
            Player2 = player2;
            AimLines = aimLines;
        }
    }
}